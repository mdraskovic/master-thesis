### Utils for loading data
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
plt.rcParams['axes.grid'] = True
import math

font = {'size'   : 14} # Increase size of figure labes
matplotlib.rc('font', **font)

# Set flags
do_plot_csv = False
do_show_simulation = False
do_plot_suboptimal = False         # A lot of plots!!!
do_plot_suboptimal_states = False  # A lot of plots!!!
cluster_folder="cluster_foler" # Change to eg. 'cluster1' when running for target1.

# Which data is loaded
targets = [1,2,3,4,5,6,7,8,9] 
users = [1,2,3,4,5,6,7,8]   # 4,5 for producing nice plots
trials = [1,2] # 1,2

vicon_DT = 0.00125 # freq = 800 Hz
number_suboptimal = 5 # How many suboptimal trajecotries will be created for each optimal (observed) one

def distance_3D(a, b):
    """Euclidean distance between 2 points"""
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2 + (a[2] - b[2])**2)

### Plots ###
def plot_trajs_3D(shoulder_list, elbow_list, hand_list):
    """Plot [x,y,z] of shoulder, elbow and hand in cm-s
    
    ----Parameters----
    shoulder, elbow, hand : [x,y,z] positions of joints in meters for all targets"""
    fig = plt.figure()
    fig.suptitle("3D trajectories")
    ax = fig.add_subplot(111, projection = '3d')

    for i in range(len(shoulder_list)):
        shoulder = shoulder_list[i]
        elbow = elbow_list[i]
        hand = hand_list[i]

        ax.plot(shoulder[:,0]*100, shoulder[:,1]*100, shoulder[:,2]*100, color='xkcd:grass green')
        ax.scatter(*shoulder.T[:,0]*100, color = 'xkcd:grass green') # Mark starting point
        ax.plot(elbow[:,0]*100, elbow[:,1]*100, elbow[:,2]*100, color='xkcd:light green')
        ax.scatter(*elbow.T[:,0]*100, color = 'xkcd:light green')
        ax.plot(hand[:,0]*100, hand[:,1]*100, hand[:,2]*100, color='xkcd:brick red')
        ax.scatter(*hand.T[:,0]*100, color = 'xkcd:brick red')
        ax.set_xlabel('x [cm]'), ax.set_ylabel('y [cm]'), ax.set_zlabel('z [cm]')

    pa1 = Patch(facecolor='xkcd:grass green'); pa2 = Patch(facecolor='xkcd:light green'); pa3 = Patch(facecolor='xkcd:brick red')
    handles = [pa1, pa2, pa3]; labels=['Shoulder', 'Elbow', 'Hand']
    ax.legend(handles=handles,labels=labels, ncol=1, handletextpad=0.5, handlelength=2, handleheight=0.2, columnspacing=-0.5)

def plot_eef_3D(hand_list):
    """Plot [x,y,x] of a hand in cm-s"""
    fig = plt.figure()
    fig.suptitle("3D trajectories")
    ax = fig.add_subplot(111, projection = '3d')
    ccc=['xkcd:avocado', 'xkcd:slime green', 'xkcd:emerald green', 'xkcd:apple green', 'xkcd:grass']

    for i in range(len(hand_list)):
        hand = hand_list[i]
        ax.plot(hand[:,0]*100, hand[:,1]*100, hand[:,2]*100, color=ccc[i%len(ccc)])
        ax.scatter(*hand.T[:,0]*100, color=ccc[i%len(ccc)])

    ax.set_xlabel('x [cm]'), ax.set_ylabel('y [cm]'), ax.set_zlabel('z [cm]')
    pa1 = Patch(facecolor=ccc[0]); pa2 = Patch(facecolor=ccc[1]); pa3 = Patch(facecolor=ccc[2])
    handles = [pa1, pa2, pa3]; labels=['Original trajectories']
    fig.legend(handles=handles,labels=labels, ncol=3, handletextpad=0.5, handlelength=1,handleheight=0.3, columnspacing=-0.5)
    
def plot_torque_peaks(dataset, dataset_eef):
    """Plot umax and umin for all joints
    
    ----Parameters----
    dataset : (Shape len(targets) x T x 5) joints torques/controls
    dataset_eef: (Shape len(targets) x T x 3) end effector 3D trajectory, used to calculate distance from initial position to target"""
    fig, ax = plt.subplots(1, 4, figsize=plt.figaspect(0.3))
    ccc=['xkcd:avocado', 'xkcd:slime green', 'xkcd:emerald green', 'xkcd:apple green', 'xkcd:grass']
    joint_names = ["Shoulder Yaw","Shoulder Pitch","Shoulder Roll","Elbow Pitch",""]

    # Add point for each trajectory in dataset
    for n in range(len(dataset)):
        data = dataset_eef[n]                             # Isolate one eef dataset row
        distance = distance_3D(data[0,:], data[-1,:])     # distance to target from initial position
        data = dataset[n]                                 # Isolate ona dataset row    

        for j in range(4):
            umax = max(data[:,j])
            umin = min(data[:,j])
            ax[j].scatter(distance, umax, linewidth=1.0, color=ccc[n%len(ccc)])
            ax[j].scatter(distance, umin, linewidth=1.0, color=ccc[n%len(ccc)])
            ax[j].vlines(distance, umin, umax, color=ccc[n%len(ccc)], alpha=0.4, linewidth=4.0)
            ax[j].set(xlabel=r'Distance to target [m]', title=joint_names[j])
    ax[0].set(ylabel=r"Torque range [Nm]")

    fig.savefig("torque_peaks.pdf")
    # fig.suptitle('Torque peaks')

def plot_states(dataset, viconDT):
    """Plot angle and velocity profiles for all joints
    
    ----Parameters----
    dataset : (Shape len(targets) x T+1 x 10) joints states = [q, q']"""
    fig, ax = plt.subplots(2, 4, figsize=plt.figaspect(0.3))
    duration = dataset[0].shape[0]
    ccc=['xkcd:avocado', 'xkcd:slime green', 'xkcd:emerald green', 'xkcd:apple green', 'xkcd:grass']
    joint_names=["Shoulder Yaw","Shoulder Pitch","Shoulder Roll","Elbow Pitch"]
    rad_to_deg = 180.0/3.141592653589793  
    t = np.array(range(duration))*viconDT

    for n in range(len(dataset)):
        data = dataset[n]

        for j in range(4):
            ax[0,j].plot(t, data[:,j]*rad_to_deg, linewidth=1.0, color=ccc[n%len(ccc)])
            ax[0,j].set(title=joint_names[j])
            ax[1,j].plot(t, data[:,5+j]*rad_to_deg, linewidth=1.0, color=ccc[n%len(ccc)])
            ax[1,j].set(xlabel=r"Time [s]")
    
    ax[0,0].set(ylabel = r'Position [deg]')
    ax[1,0].set(ylabel = r'Velocity [$\frac{\mathrm{deg}}{\mathrm{s}}$]')

    fig.savefig("data_states.pdf")
    # fig.suptitle("Joint Position & Velocity ")

def plot_normalization(dataset, normalized_dataset, DT, vicon_DT):
    """Plot comparison between time invariant and original data
    
    ----Parameters----
    dataset : (Shape len(targets) x T x 3) X,Y,Z positions of joints in meters for all targets"""
    fig, ax = plt.subplots(2,3, figsize=plt.figaspect(0.4))
    ccc=['xkcd:avocado', 'xkcd:slime green', 'xkcd:emerald green', 'xkcd:apple green', 'xkcd:grass']

    for i in range(len(dataset)): # Add each trajectory in dataset
        data = dataset[i]                             # Isolate one dataset row
        normalized_data = normalized_dataset[i]
        duration = data.shape[0]#*vicon_DT            # Movement duration (number of knots*timestep)
        normalized_duration = normalized_data.shape[0]

        t = np.array(range(duration))*vicon_DT
        normalized_t = np.array(range(normalized_duration))*DT
        for j in range(3):
            ax[0,j].plot(t, data[:,j], linewidth=1.0, color=ccc[i%len(ccc)])
            ax[1,j].plot(normalized_t, normalized_data[:,j], linewidth=1.0, color=ccc[i%len(ccc)])

    ax[0,0].set_ylabel(r'Original EEF [m]')
    ax[1,0].set_ylabel(r'Time invariant EEF [m]')

    ax[1,0].set_xlabel(r"Time [s]")
    ax[1,1].set_xlabel(r"Time [s]")
    ax[1,2].set_xlabel(r"Time [s]")

    ax[0,0].set_title('$x$ axis')
    ax[0,1].set_title('$y$ axis')
    ax[0,2].set_title('$z$ axis')

    fig.savefig("data_normalization.pdf")
    # fig.suptitle('Original and Time-Invariant Data')