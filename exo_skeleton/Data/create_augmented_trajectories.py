### Create augmented trajectories from the optimal ones, used for IOC for MPC.
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import numpy as np
import pinocchio as pin
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
import utils
from IOC.OCP import OptimalControlProblem

font = {'size'   : 14} # Increase size of figure labes
matplotlib.rc('font', **font)

#################################################
#------------------ Setup data ------------------
#################################################
cluster_folder = ["cluster5"]#,"cluster2","cluster3","cluster4","cluster5","cluster6","cluster7","cluster8","cluster9"]
target_number = [7] #range(0,16)

# Set flags
do_plot = True         # A lot of plots !!!

# Create robot arm
DT = utils.DT 
T = utils.T
robot_urdf = utils.robot_urdf
mesh_dir = utils.mesh_dir
target_urdf = utils.target_urdf
end_effector = utils.end_effector
model, _, _  = pin.buildModelsFromUrdf(robot_urdf, mesh_dir)
data = model.createData()
ocp = OptimalControlProblem(model, data, DT, T, end_effector)

#################################################
#---------- Load optimal trajectories -----------
#################################################
# Create a folder
if not os.path.exists('MPC'):
    os.mkdir('MPC')

# Init lists for start and end positions   
new_target_sequence = np.zeros((T, 3))
new_x0_sequence = np.zeros((T, 10))

for j in cluster_folder:                                    # j : Go through all clusters
    # Get init and target eef position
    target_sequence = np.loadtxt(j + '/target_sequence.txt', comments="#", delimiter=" ", unpack=False)
    x0_sequence = np.loadtxt(j + '/x0_sequence.txt', comments="#", delimiter=" ", unpack=False)
    target_sequence = target_sequence[target_number]     # Get specific target from the sequence
    x0_sequence = x0_sequence[target_number]

    # Create output folder
    name = 'MPC/' + str(j) 
    if not os.path.exists(name):
        os.mkdir(name)

    for i in range(len(target_sequence)):                # i : Go through all targets in a cluster
        cnt = 0

        # Get reference trajectories
        trajectories_opt = np.zeros((T+1, 10))           # Initialize matrices to store augmented trajectories
        folder_name = j +'/target'+str(target_number[i]) # Open folder for each target 
        trajectory_opt = np.loadtxt(folder_name+'/traj_OCP.txt', comments="#", delimiter=" ", unpack=False)

        # Setup plot
        if do_plot:
            nn = 0  # Counter for augmented trajectories in the plot
            fig = plt.figure()
            t = np.arange(0, T+1)*DT
            plt.xlabel(r'Time [s]'), plt.ylabel(r'$x$ axis [cm]')
            colors = ['red', 'blue']
            style = ['solid', 'dotted']
            pa1 = Patch(facecolor='red', alpha=0.5); pa2 = Patch(facecolor='blue', alpha=0.5) # Create label
            pa3 = Patch(facecolor='green', alpha=0.6)
            plt.legend(handles=[pa1, pa3, pa2, pa3],
                        labels=['','', 'Augmented trajectories', 'Optimal trajectory'],
                        ncol=2, handletextpad=0.5, handlelength=1,handleheight=0.3, columnspacing=-0.5, loc = 'lower right')

        target = target_sequence[i]
        x0 = x0_sequence[i]

        # Create output subfolder
        name = 'MPC/' + str(j) + '/target' + str(i)      
        if not os.path.exists(name):
            os.mkdir(name)

        # Write the optimal trajectory
        name = 'MPC/' + str(j) + '/target' + str(i) + '/shift0'
        if not os.path.exists(name):
            os.mkdir(name)
        np.savetxt(name +'/traj_OCP.txt', trajectory_opt)

        new_target_sequence[cnt,:] = target              # Target is unchanged
        new_x0_sequence[cnt,:] = x0                      # Initial state is one node moved in the horizon
        cnt += 1

        #################################################
        #-------- Create augmented trajectories ---------
        #################################################
        for n in range(1, T): # n : Go through all points in a trajectory
            # Initialize where MPC trajs will be saved
            MPC_traj = np.zeros((T+1, 10))  # Initialize matrice to store MPC trajectory

            # Target and x0
            new_target_sequence[cnt,:] = target          #Target is unchanged
            new_x0_sequence[cnt,:] = trajectory_opt[n,:] # Init state is the one along the trajectory but shifted

            # MPC traj
            MPC_traj[0:T-n+1] = trajectory_opt[n:]
            MPC_traj[T-n+1:] = trajectory_opt[-1]        # Set final joint states to the last one
            MPC_traj[T-n:, 5:] = 0                       # Set final velocities to 0

            # Save trajectory
            name = 'MPC/' + str(j) + '/target' + str(i) + '/shift'+str(n)
            if not os.path.exists(name):
                    os.mkdir(name)
            np.savetxt(name +'/traj_OCP.txt', MPC_traj)
            cnt += 1

            # Plot every 10th augmented trajectory
            if do_plot and n%10==0:
                t = np.arange(n, T+1+n)*DT
                MPC_eef = ocp.get_eef(MPC_traj)
                plt.plot(t, MPC_eef[:,0]*100, alpha = 0.5, color = colors[nn], linewidth=7+nn*2, linestyle=style[nn]) # Increase thickness
                nn += 1

        # Plot optimal (the original) trajectory
        if do_plot:
            t = np.arange(0, T+1)*DT
            eef_opt = ocp.get_eef(trajectory_opt)
            plt.plot(t, eef_opt[:,0]*100, alpha=0.6, linewidth=4, color='green')
            fig.suptitle("Augmented Trajectories")
            # fig.savefig("MPC_trajs.pdf")

        # Save sequences of initial states and targets
        np.savetxt('MPC/' + str(j) + '/target' + str(i) + '/target_sequence.txt', new_target_sequence)
        np.savetxt('MPC/' + str(j) + '/target' + str(i) + '/x0_sequence.txt', new_x0_sequence)
plt.show()   