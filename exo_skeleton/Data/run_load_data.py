### Load recorded reaching data and transform it to state and control trajectories
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) # Add parent folder to path

import pybullet as p
import numpy as np
import time
import matplotlib.pyplot as plt
import pinocchio as pin
from IOC.OCP import OptimalControlProblem
import data_utils
from MPC.simulators.armSimulator import ArmSimulator
from MPC.simulators.targetSimulator import TargetSimulator
import utils 
import load_data

#################################################
#------------------ Setup data ------------------
#################################################
do_plot_csv = data_utils.do_plot_csv
do_show_simulation = data_utils.do_show_simulation
do_plot_suboptimal = data_utils.do_plot_suboptimal         
do_plot_suboptimal_states = data_utils.do_plot_suboptimal_states 
cluster_folder = data_utils.cluster_folder 

vicon_DT = data_utils.vicon_DT
number_suboptimal = data_utils.number_suboptimal

# Which data is loaded
targets = data_utils.targets
users = data_utils.users
trials = data_utils.trials

# Create robot arm
DT = utils.DT
T = utils.T
robot_urdf = utils.robot_urdf
mesh_dir = utils.mesh_dir
target_urdf = utils.target_urdf
end_effector = utils.end_effector
model, _, _ = pin.buildModelsFromUrdf(robot_urdf, mesh_dir)
data = model.createData()
frame_id = model.getFrameId(end_effector)
ocp = OptimalControlProblem(model, data, DT, T, end_effector)

# Start simulation
if do_show_simulation:
    pybDT = utils.pybDT
    p.connect(p.GUI)
    p.setGravity(0, 0, -9.81)
    p.setPhysicsEngineParameter(fixedTimeStep=pybDT, numSubSteps=1)
    joint_names = utils.joint_names
    simulator = ArmSimulator(DT, robot_urdf, mesh_dir, joint_names, pybDT)

#################################################
#----------------- Load csv data ----------------
#################################################
# Collected values for all targets. Used for plots
shoulder_list = []
elbow_list = []
hand_list = []          # Shape: len(filenames) x T x 3

norm_shoulder_list = [] # Time invariant length
norm_elbow_list = []
norm_hand_list = []

q_list = []
u_list = []

# Load file names
filenames = []
for target in targets:
    for user in users:
        for trial in trials:
            name = 'CSVfiles/target'+str(target)+'/user'+str(user)+'/trial'+str(trial)+'.csv'
            filenames.append(name)

# Init lists for start and end positions            
target_sequence = np.zeros((len(filenames), 3))
x0_sequence = np.zeros((len(filenames), 10))

# Load data to arrays
current_dir = os.path.dirname(__file__)
for n in range(len(filenames)):
    print("Dataset ", filenames[n])
    filename = os.path.join(current_dir, filenames[n])
    csv_data = load_data.load_csv(filename)
    shoulder, elbow, hand = load_data.csv_to_arrays(csv_data) # Shape length x 3

    # Save data to a list of all data
    shoulder_list.append(shoulder)
    elbow_list.append(elbow)
    hand_list.append(hand)

    shoulder, elbow, hand = load_data.normalize_length(shoulder, elbow, hand, DT, T, vicon_DT)
    norm_shoulder_list.append(shoulder)
    norm_elbow_list.append(elbow)
    norm_hand_list.append(hand)

    #################################################
    #------------ Get states and controls -----------
    #################################################
    # Get all states and controls
    q_init = load_data.inverse_kinematics_two_frames(model, data, hand[0], elbow[0], DT)
    q, u = load_data.inverse_kinematics_DDP(model, hand, elbow, DT, T, q_init) 
    x0_sequence[n, :] = q_init     # Save start position
    q_list.append(q)
    u_list.append(u)

    # Compute EEF 
    pin.forwardKinematics(model, data, q[-1][0:5], q[-1][5:])
    pin.updateFramePlacements(model, data)
    target = np.array(data.oMf[frame_id].translation.T.flat)
    target_sequence[n, :] = target # Save end position

    #################################################
    #-------- Create suboptimal trajectories --------
    #################################################
    if not os.path.exists(cluster_folder):
        os.mkdir(cluster_folder)
    folder_name = cluster_folder + '/target'+str(n)     # Create folder for each target 
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)

    ocp.save_trajectories(q_init, target, q, u, folder_name, number_suboptimal, do_plot_suboptimal, do_plot_suboptimal_states)

    if do_show_simulation:
        target_shoulder = TargetSimulator(target, target_urdf)
        target_elbow = TargetSimulator(target, target_urdf) 
        target_hand = TargetSimulator(target, target_urdf)
        for i in range(T):
            target_shoulder.change(shoulder[i,:])
            target_elbow.change(elbow[i,:])
            target_hand.change(hand[i,:])
            simulator.reset(q[i,:])
            time.sleep(1/100)

if do_plot_csv:
    data_utils.plot_normalization(hand_list, norm_hand_list, DT, vicon_DT)
    # Original data lengths
    data_utils.plot_eef_3D(hand_list)
    data_utils.plot_torque_peaks(u_list, hand_list)
    # Time invariant data
    data_utils.plot_states(q_list, DT)

np.savetxt(cluster_folder+'/x0_sequence.txt', x0_sequence)
np.savetxt(cluster_folder+'/target_sequence.txt', target_sequence)
plt.show()