### Help functions for loading recorded reaching trajectories
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

from csv import reader
import numpy as np
import crocoddyl
import pinocchio as pin
import MPC.ddp.weights as weights

def load_csv(filename):
    with open(filename, 'r') as read_obj: # Open file in read mode
        csv_reader = reader(read_obj)     # Load csv data
        data = np.array(list(csv_reader)) 
    return data

def csv_to_arrays(data):
    """Transform data from csv file to numpy arrays for shoulder, elbow and hand.

    ----Parameters----
    data : csv object with shoulder, elbow and hand X,Y,Z positions
    
    ----Returns------
    shoulder, elbow, hand : X,Y,Z positions of joints in meters
    """    
    column_elbow = 5                      # Where specific data starts in csv files
    column_shoulder = 11
    column_hand = 17
    data = data[5:]                       # Remove header 

    d2 = np.zeros((len(data)-1, len(data[0]))) 
    for i in range(len(data)-1):          # Define a new array bcs 'data' is a object type now
        d2[i,:] = data[i]

    number_of_points = d2.shape[0]        # Length of data
    shoulder = np.zeros((number_of_points, 3))
    elbow = np.zeros((number_of_points, 3))
    hand = np.zeros((number_of_points, 3))

    shoulder[:,0] = d2[:,column_shoulder] # Data in mm
    shoulder[:,1] = d2[:,column_shoulder+1]
    shoulder[:,2] = d2[:,column_shoulder+2]
    elbow[:,0] = d2[:,column_elbow]
    elbow[:,1] = d2[:,column_elbow+1]
    elbow[:,2] = d2[:,column_elbow+2]
    hand[:,0] = d2[:,column_hand]
    hand[:,1] = d2[:,column_hand+1]
    hand[:,2] = d2[:,column_hand+2]

    # Shift coordinate center to zero
    center = np.array([shoulder[0,0], shoulder[0,1], shoulder[0,2]]) 
    shoulder -= center
    elbow -= center
    hand -= center
    shoulder *= 0.001                     # Convert to m
    elbow *= 0.001
    hand *= 0.001
    return shoulder, elbow, hand

### Inverse kinematics ###
def inverse_kinematics_two_frames(model, data, p_des1, p_des2, DT):
    """Get joint angles (q) based on the end-effector positions of 2 links
    
    ----Parameters----
    model, data : from Pinocchio instance
    end_effector : last link in the system -> "hand"
    p_des1, p_des2 : Hand and elbow desired positions"""
    frame_id1 = model.getFrameId("hand")
    frame_id2 = model.getFrameId("elbow")

    q  = np.array([0.0, -0.261799, 0.0, -0.523599, 0.0]) # Initial guess
    eps    = 0.01
    max_it = 1000
    alpha = 0.1
    
    for i in range(max_it):
        J1 = jacobian(model, data, q, frame_id1)
        J2 = jacobian(model, data, q, frame_id2)
        J = np.vstack((J1, J2))
        
        p_des = np.concatenate((p_des1, p_des2))

        p1 = forward_kinematics(model, data, q, frame_id1)
        p2 = forward_kinematics(model, data, q, frame_id2)
        p = np.concatenate((p1, p2))
        
        err = p - p_des
        if np.linalg.norm(err) <= eps:
            v = np.array([0,0,0,0,0])
            break
        
        v = - alpha * np.linalg.pinv(J) @ err  
        q = pin.integrate(model, q, v * DT)
    q = np.concatenate((q,v))  
    return q

def inverse_kinematics(model, data, end_effector, p_des):
    """Get joint angles (q) based on the end-effector position
    
    ----Parameters----
    model, data : from Pinocchio instance
    end_effector : last link in the system -> "hand"
    p_des : End-effector position"""
    frame_id = model.getFrameId(end_effector)
    
    q  = np.array([0.0, -0.261799, 0.0, -0.523599, 0.0]) # Initial guess
    eps    = 0.01
    max_it = 1000
    alpha = 0.1
    
    for i in range(max_it):
        p = forward_kinematics(model,data, q, frame_id)
        err = np.linalg.norm(p - p_des)
        if err <= eps:
            break
        J = jacobian(model,data, q, frame_id)
        q = q - alpha * pinv(J) @ (forward_kinematics(model, data, q, frame_id) - p_des)

    q = np.concatenate((q, np.array([0,0,0,0,0])))  
    return q

def forward_kinematics(model, data, q, frame_id):
    """Get the end-effector translation [x,y,z] of the link with id frame_id, based on the joint angles (q) """
    pin.forwardKinematics(model, data, q)
    pose = pin.updateFramePlacement(model, data, frame_id)
    return pose.translation

def jacobian(model, data, q, frame_id):
    """Calculate the derivative of the forward kinematics function of a frame woth frame_id, at a given configuration q"""
    pin.framesForwardKinematics(model, data, q)
    pose = pin.updateFramePlacement(model, data, frame_id)
    body_jacobian = pin.computeFrameJacobian(model, data, q, frame_id)
    Ad = np.zeros((6, 6))
    Ad[:3, :3] = pose.rotation
    Ad[3:, 3:] = pose.rotation
    J = Ad @ body_jacobian
    Jlin = J[:3, :]
    return Jlin

def pinv(J, eps=1e-3):
    """Calculate the right pseudoinverse of a matrix J"""
    JJT = J @ J.T
    return J.T @ np.linalg.inv((JJT + eps * np.eye(*JJT.shape)))

def inverse_kinematics_DDP(model, h, e, DT, T, x0):
    """Get state and control from 3D coordinates of elbow and hand, using DDP.
    Place high frame translation costs on each node in the horizon.
    Better version than iterative inverse kinematics because it also returns torques 
    (so no need for inverse dynamics) and because the resulting trajectory is smoother."
    
    ----Parameters----
    model : from Pinocchio instance
    h  : hand [x,y,z]  - matrix of all positions in the horizon
    e  : elbow [x,y,z] - matrix of all positions in the horizon
    DT, T : timestep and horizon
    x0 : Initial state [q,v]
    
    ----Returns------
    q : state, joint positions and velocities
    u : control torque"""
    nq = model.nq
    x_ref, _, _, uReg_w, xReg_w, frame_w = weights.load(nq, nq)
    state = crocoddyl.StateMultibody(model)
    actuation = crocoddyl.ActuationModelFull(state)
    nu = actuation.nu
    
    # Add high frame translation cost for each position in the OCP horizon
    uReg=0.001
    xReg=0.01
    frame_id_hand = model.getFrameId("hand")
    frame_id_elbow = model.getFrameId("elbow")
    running_models = [] 
    for j in range(T):
        frameResidual = crocoddyl.ResidualModelFrameTranslation(state, frame_id_hand, h[j,:]) # Hand frame cost
        frameActivation = crocoddyl.ActivationModelWeightedQuad(frame_w) 
        frameCost_hand = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)

        frameResidual = crocoddyl.ResidualModelFrameTranslation(state, frame_id_elbow, e[j,:]) # Elbow frame cost
        frameCost_elbow = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)

        uRegResidual = crocoddyl.ResidualModelControlGrav(state, nu) # Regularization to help DDP converge
        uActivation = crocoddyl.ActivationModelWeightedQuad(uReg_w)
        uRegCost = crocoddyl.CostModelResidual(state, uActivation, uRegResidual) 

        xRegResidual = crocoddyl.ResidualModelState(state, x_ref, nu)
        xActivation = crocoddyl.ActivationModelWeightedQuad(xReg_w)
        xRegCost = crocoddyl.CostModelResidual(state, xActivation, xRegResidual)   

        xLimitResidual = crocoddyl.ResidualModelState(state, np.zeros(nq+nq), nu)
        xLimitActivation = crocoddyl.ActivationModelQuadraticBarrier(crocoddyl.ActivationBounds(state.lb, state.ub))
        xLimitCost = crocoddyl.CostModelResidual(state, xLimitActivation, xLimitResidual)

        runningCostModel = crocoddyl.CostModelSum(state)
        runningCostModel.addCost("frameCostHand", frameCost_hand, 10)
        runningCostModel.addCost("frameCostElbow", frameCost_elbow, 1)  
        runningCostModel.addCost("uRegCost", uRegCost, uReg) 
        runningCostModel.addCost("xRegCost", xRegCost, xReg)
        runningCostModel.addCost("xLimitCost", xLimitCost, 10) 

        running_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel)
        running_model = crocoddyl.IntegratedActionModelEuler(running_DAM, DT)
        running_model.differential.armature = np.array([.1]*nq)
        running_models.append(running_model)

    frameResidual = crocoddyl.ResidualModelFrameTranslation(state, frame_id_hand, h[-1,:])
    frameActivation = crocoddyl.ActivationModelWeightedQuad(frame_w) 
    frameCost_hand = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)

    frameResidual = crocoddyl.ResidualModelFrameTranslation(state, frame_id_elbow, e[-1,:])
    frameCost_elbow = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)

    terminalCostModel = crocoddyl.CostModelSum(state)
    terminalCostModel.addCost("frameCostHand", frameCost_hand, 10) 
    terminalCostModel.addCost("frameCostElbow", frameCost_elbow, 1) 
    terminalCostModel.addCost("xLimitCost", xLimitCost, 10) 
    terminal_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, terminalCostModel)
    terminal_model = crocoddyl.IntegratedActionModelEuler(terminal_DAM)
    terminal_model.differential.armature = np.array([.1]*nq)

    # Create and solve shooting problem
    problem = crocoddyl.ShootingProblem(x0, running_models, terminal_model)
    ddp = crocoddyl.SolverDDP(problem)
    tf = ddp.solve()
    q = np.array(ddp.xs)
    u = np.array(ddp.us)
    return q, u

### Study the data ###
def normalize_length(shoulder, elbow, hand, DT, T, vicon_DT):
    """Change the time of demonstration to be between zero and T, in order to achieve duration invariance.

    ----Parameters----
    shoulder, elbow, hand : [x,y,z]] positions of joints in meters for all targets
    DT, T : timestep and horizon
    vicon_DT : timestep of motion capture system"""
    # Cut length of data
    duration_in_sec = DT*(T+1)
    required_length = int(duration_in_sec/vicon_DT)
    shape = shoulder.shape[0]

    if shape>=required_length:
        shoulder = shoulder[0:required_length, :]
        elbow = elbow[0:required_length, :]
        hand = hand[0:required_length, :]
    else: # If the task is not long enough, repeat the last value
        new_shoulder = np.zeros((required_length,3))
        new_elbow = np.zeros((required_length,3))
        new_hand = np.zeros((required_length,3))
        new_shoulder[0:shape, :] = shoulder
        new_elbow[0:shape, :] = elbow
        new_hand[0:shape, :] = hand
        for i in range(shape, required_length):
            new_shoulder[i, :] = shoulder[-1,:]
            new_elbow[i, :] = elbow[-1,:]
            new_hand[i, :] = hand[-1,:]
        shoulder = new_shoulder.copy()
        elbow = new_elbow.copy()
        hand = new_hand.copy()

    # Change step of data
    step = int(DT/vicon_DT)
    ret_shoulder = np.zeros((T+1,3))
    ret_elbow = np.zeros((T+1,3))
    ret_hand = np.zeros((T+1,3))

    cnt = 0
    for i in range(shoulder.shape[0]):
        if (i%step == 0):
            ret_shoulder[cnt,:] = shoulder[i,:]
            ret_elbow[cnt,:] = elbow[i,:]
            ret_hand[cnt,:] = hand[i,:]
            cnt += 1
    return ret_shoulder, ret_elbow, ret_hand

def get_mean_trajectory(shoulder_list, elbow_list, hand_list):
    """Calculate mean trajectory from all of them.
    
    ----Parameters----
    shoulder, elbow, hand : [x,y,z] normalized positions of joints in meters for all targets"""
    shape = shoulder_list[0].shape
    shoulder_mean = np.zeros(shape)
    elbow_mean = np.zeros(shape)
    hand_mean = np.zeros(shape)
    number_of_data = len(shoulder_list)

    for i in range(number_of_data):
        shoulder_mean += shoulder_list[i]
        elbow_mean += elbow_list[i]
        hand_mean += hand_list[i]

    shoulder_mean = shoulder_mean/number_of_data
    elbow_mean = elbow_mean/number_of_data
    hand_mean = hand_mean/number_of_data
    return shoulder_mean, elbow_mean, hand_mean