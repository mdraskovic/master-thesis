### Variables used across the whole project
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import numpy as np
import crocoddyl
import pinocchio as pin
import torch
from torch.utils.data import Dataset
from torch import nn
torch.Generator().manual_seed(42)

DT = 0.05 # Crocoddyl DT 0.01 for 1 OCP
T = 30
robot_urdf = "/home/mdraskovic/teza/exo_skeleton/URDF/arm.urdf"
mesh_dir = "/home/mdraskovic/teza/exo_skeleton/URDF"
target_urdf = "/home/mdraskovic/teza/exo_skeleton/URDF/target.urdf"
end_effector = "hand"
pybDT = 0.001
joint_names = ["shoulder_joint_x","shoulder_joint_y","shoulder_joint_z","elbow","wrist"]
arm_urdf = "/home/mdraskovic/teza/exo_skeleton/URDF/arm.urdf"
exo_urdf = "/home/mdraskovic/teza/exo_skeleton/URDF/exoskeleton.urdf"

### Functions ###
def create_w(w):
    """De-normalize weights from weight vector w. 
    Constants are empirically defined. 
    Weight vector w is coming from a NN model."""
    running_w = np.array([w[0], 0.1*w[1], 0.001, 1000, 1000, 0.00, 0.001*w[2]]) #frameTrans, xReg, uReg, xLim, uLim, energy, accel
    terminal_w = np.array([1000*w[2], 0.1*w[1], 1000.0]) #frameTranslation, xReg, xLim
    return running_w, terminal_w 

def get_element(data, row, column):
    """Get element in the [row,column] position of data file"""
    data[row,column] = data[row,column].replace('         ',' ') # Remove unnecessary characters
    data[row,column] = data[row,column].replace('        ',' ')
    data[row,column] = data[row,column].replace('       ',' ')
    data[row,column] = data[row,column].replace('      ',' ')
    data[row,column] = data[row,column].replace('     ',' ')
    data[row,column] = data[row,column].replace('    ',' ')
    data[row,column] = data[row,column].replace('   ',' ') 
    data[row,column] = data[row,column].replace('  ',' ') 
    data[row,column] = data[row,column].replace('[ ','') 
    data[row,column] = data[row,column].replace('] ','')
    data[row,column] = data[row,column].replace(' [','') 
    data[row,column] = data[row,column].replace(' ]','')
    data[row,column] = data[row,column].replace('[','')
    data[row,column] = data[row,column].replace(']','') 
    mid =  [e for e in data[row,column].split(' ')] # Split the string into list elemnts
    ret = np.array([float(e) for e in mid])          # Convert to float numpy array
    return ret

### Custom Crocoddyl cost residuals ###
class energyResidual(crocoddyl.ResidualModelAbstract):
    """C = sum |theta' * tau|, where theta are joint angles and tau are torques.
    Define cost like this:
        energyRes = energyResidual(state, nu)
        energyActivation = crocoddyl.ActivationModelSmooth1Norm(nu)
        energyCost = crocoddyl.CostModelResidual(state, energyActivation, energyRes)"""
    def __init__(self, state, nu):
        nr = 5
        super().__init__(state, nr, nu)
        self.nq = nr
        self.nx = 2* self.nq

    def calc(self, data, x, u):
        r = [x[self.nq+i] * u[i] for i in range(self.nq)]
        data.r = np.array(r)
    
    def calcDiff(self, data, x, u):
        Rx = np.zeros((self.nq, self.nx))
        Rx[:, self.nq:] = np.diag(u)
        Ru = np.diag(x[self.nq:])
        data.Rx = Rx
        data.Ru = Ru

class jointAccelerationResidual(crocoddyl.ResidualModelAbstract):
    """C = sum (theta' ^2)
    where theta are joint angles."""
    def __init__(self, state, nu, model, dataa):
        nr = 5
        super().__init__(state, nr, nu)
        self.nq = nr
        self.nx = 2* self.nq
        self.model = model
        self.dataa = dataa

    def calc(self, data, x, u):
        r = pin.aba(self.model, self.dataa, x[:self.nq], x[self.nq:], u)
        data.r = r
    
    def calcDiff(self, data, x, u):
        dq, dv, dtau = pin.computeABADerivatives(self.model, self.dataa, x[:self.nq], x[self.nq:], u)
        data.Rx = np.concatenate((dq,dv), axis=1)
        data.Ru = dtau

### NN Model ###
class WeightsModel(nn.Module):
    def __init__(self, input_size, output_size):
        # game params
        super().__init__()
        
        self.swish = nn.SiLU()
        self.relu = nn.ReLU()
        self.fc1 = nn.Linear(input_size, 256)
        self.bn1 = nn.BatchNorm1d(256)

        self.fc2 = nn.Linear(256, 512)
        self.bn2 = nn.BatchNorm1d(512)

        self.fc3 = nn.Linear(512, 256)
        self.bn3 = nn.BatchNorm1d(256)

        self.out = nn.Linear(256, output_size)
        
    def forward(self, x):
        if len(x.shape) == 1:
            x = x[None, :]
        x = self.swish(self.bn1(self.fc1(x)))
        x = self.swish(self.bn2(self.fc2(x)))
        x = self.swish(self.bn3(self.fc3(x)))
        x = self.out(x)
        return x

    def save(self, path):
        torch.save(self.state_dict(), path)

    def load(self, path, device='cpu'):
        self.load_state_dict(torch.load(path, map_location=torch.device(device)))

class ArmDataset(Dataset):
    """Custom Dataset class"""
    def __init__(self, x, y):
        """x = [q, q', target] ; y = w (weights; labels)"""
        self.x = x
        self.y = y
        
    def __len__(self):
        return self.x.shape[0]

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]