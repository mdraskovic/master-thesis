### Utils for IOC
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.lines import Line2D

plt.rc('axes', labelsize=14)    # fontsize of the x and y labels

cluster_folder = ["cluster1","cluster2","cluster3","cluster4","cluster5","cluster6","cluster7","cluster8","cluster9"]
target_number = range(5,16) # 0,16 == 0 -> 15
shift_number = range(0,30)  # 0,30 == 0 -> 29
save_file = 'dummy.csv'

### Plots ###
def iteration_steps(iterations_w, iterations_objective_function):
    """Plot weights and objective function values through iterations."""
    # Plot weights
    t = np.arange(0, len(iterations_w), 1)
    w_it = np.array(iterations_w)
    dimension = len(w_it[0])
    fig, axs = plt.subplots(1, dimension)
    fig.suptitle("Weights")
    for n in range(dimension):                         
        axs[n].plot(t, w_it[:,n], linewidth=1.0, color='black')  
        axs[n].scatter(t, w_it[:,n], color='black')
        axs[n].set(xlabel='Iteration', ylabel='w['+str(n)+']')  

    # Plot objective function
    fig2, axs2 = plt.subplots()
    t = np.arange(0, len(iterations_objective_function), 1) # Plot objective function
    axs2.plot(t, iterations_objective_function, linewidth=1.0, color='black')  
    axs2.scatter(t, iterations_objective_function, color='black')
    axs2.set(xlabel='Iteration', ylabel='Objective function value') 
    fig2.suptitle("Objective Function Value")

def plot_eef_trajectories(eef1, eef2, title, DT, trajs_iterations=[]):
    """Plot original and recreated trajectories in 3D. 
    Plot each trajectory axis wrt time and errors between them.
    Possibly print all intermediate trajectories through optimization iterations.

    ----Parameters----
        eef1 : optimal, real eef traj
        eef2 : ioc recretaed traj
        title : used to specify which target it is
        DT    : DDP timestep
        trajs_iterations : intermediate trajectories, optional"""
    fig = plt.figure(figsize=(11,5))
    gs  = gridspec.GridSpec(2,4, width_ratios=[0.3,0.3,0.3,1 ], height_ratios=[1,0.3])
    ax = fig.add_subplot(122, projection = '3d')
    ax1 = fig.add_subplot(gs[0]); ax2 = fig.add_subplot(gs[1]); ax3 = fig.add_subplot(gs[2]) # Subplots for trajs wrt time
    ax4 = fig.add_subplot(gs[4]); ax5 = fig.add_subplot(gs[5]); ax6 = fig.add_subplot(gs[6]) # Subplots for errors
    t = np.arange(0, eef1.shape[0], 1)*DT

    # Add intermediate trajectories
    if trajs_iterations:
        alpha = 0.1
        for n in range(len(trajs_iterations)):
            traj = trajs_iterations[n]
            ax.plot(traj.T[0]*100, traj.T[1]*100, traj.T[2]*100, color='xkcd:pastel blue', alpha=alpha)
            ax1.plot(t, traj[:,0]*100, linewidth=1.0, color='xkcd:pastel blue', alpha=alpha)
            ax2.plot(t, traj[:,1]*100, linewidth=1.0, color='xkcd:pastel blue', alpha=alpha)
            ax3.plot(t, traj[:,2]*100, linewidth=1.0, color='xkcd:pastel blue', alpha=alpha)
            if alpha < 0.5:
                alpha += 0.01

    # Plot eef trajectories
    ax.plot(eef1.T[0]*100, eef1.T[1]*100, eef1.T[2]*100, color='xkcd:green apple')
    ax.scatter(*eef1[0]*100, color = 'xkcd:green apple') # Mark starting point
    ax.plot(eef2.T[0]*100, eef2.T[1]*100, eef2.T[2]*100, color='xkcd:bright blue', linestyle='dashed')
    ax.scatter(*eef2[0]*100, edgecolor = 'xkcd:bright blue', marker='D', c='none') 
    ax.set_xlabel(r'$x$ [cm]'); ax.set_ylabel(r'$y$ [cm]'); ax.set_zlabel(r'$z$ [cm]')
    
    # Plot eef trajectories wrt time and errors
    error = abs(eef1-eef2)
    for i, a, b in zip(range(3),[ax1, ax2, ax3], [ax4, ax5, ax6]):
        a.plot(t, eef1[:,i]*100, linewidth=2.0, color='xkcd:green apple')
        a.plot(t, eef2[:,i]*100, linewidth=2.0, color='xkcd:bright blue', linestyle='dashed')
        b.plot(t, error[:,i]*100, linewidth=2.0, color='xkcd:light red', linestyle='dotted')

    # Modify plot
    ax.dist = 9.5 # Modify size of 3d plot
    ax1.set(ylabel=r'EEF trajectory [cm]', title=r'$x$ axis')
    ax2.set(title=r'$y$ axis'), ax3.set(title=r'$z$ axis')
    ax4.set(xlabel=r"Time [s]", ylabel=r'Error [cm]')
    ax5.set(xlabel=r"Time [s]"), ax6.set(xlabel=r"Time [s]")
    legend = [Line2D([0], [0],  lw=2, label='Optimal trajectory', color='xkcd:green apple'),
            Line2D([0], [0],  lw=2, label='Recreated trajectory', color='xkcd:bright blue', linestyle='dashed'),
            Line2D([0], [0],  lw=2, label='Error', color='xkcd:light red', linestyle='dotted')]
    fig.legend(handles=legend, loc='lower right', ncol=3)

    # fig.savefig("ioc.pdf") 
    fig.suptitle(title)

def plot_joint_trajectories(traj1, traj2, title, DT):
    fig, ax = plt.subplots(2, 4, figsize=plt.figaspect(0.4))

    joint_names=["Shoulder Roll","Shoulder Pitch","Shoulder Yaw","Elbow Pitch"]
    rad_to_deg = 180.0/3.141592653589793  
    t = np.arange(0, traj1.shape[0], 1)*DT
    ax[0,0].set(ylabel = 'Position [deg]')
    ax[1,0].set(ylabel = r"Velocity [$\frac{\mathrm{deg}}{\mathrm{s}}$]")

    for j in range(4):
        ax[0,j].plot(t, traj1[:,j]*rad_to_deg, linewidth=2.0, color='xkcd:green apple')
        ax[0,j].plot(t, traj2[:,j]*rad_to_deg, linewidth=2.0, color='xkcd:bright blue', linestyle='dashed')
        ax[0,j].set(title=joint_names[j])

    for j in range(4):
        ax[1,j].plot(t, traj1[:,5+j]*rad_to_deg, linewidth=2.0, color='xkcd:green apple')
        ax[1,j].plot(t, traj2[:,5+j]*rad_to_deg, linewidth=2.0, color='xkcd:bright blue', linestyle='dashed')
        ax[1,j].set(xlabel=r"Time [s]")
    
    # fig.suptitle(title + ": Joint Position & Velocity ")
    legend = [Line2D([0], [0],  lw=2, label='Optimal', color='xkcd:green apple'),
            Line2D([0], [0],  lw=2, label='Recreated', color='xkcd:bright blue', linestyle='dashed')]
    fig.legend(handles=legend, ncol = 2, loc="upper center")
    
def plot_controls(traj1, traj2, title, DT):
    fig, ax = plt.subplots(1, 4, figsize=plt.figaspect(0.4))

    joint_names=["Shoulder Roll","Shoulder Pitch","Shoulder Yaw","Elbow Pitch"]
    rad_to_deg = 180.0/3.141592653589793  
    t = np.arange(0, traj1.shape[0], 1)*DT
    ax[0].set(ylabel = r"$\tau$ [Nm]")

    for j in range(4):
        ax[j].plot(t, traj1[:,5+j]*rad_to_deg, linewidth=2.0, color='xkcd:green apple')
        ax[j].plot(t, traj2[:,5+j]*rad_to_deg, linewidth=2.0, color='xkcd:bright blue',  linestyle='dashed')
        ax[j].set(title=joint_names[j])
        ax[j].set(xlabel=r"Time [s]")
    
    #  fig.suptitle(title + ": Control Torque ")
    legend = [Line2D([0], [0],  lw=2, label='Optimal', color='xkcd:green apple'),
            Line2D([0], [0],  lw=2, label='Recreated', color='xkcd:bright blue', linestyle='dashed')]
    fig.legend(handles=legend, ncol = 2, loc="upper center")