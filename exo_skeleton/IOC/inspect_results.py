### Plot original and trajectories reproduced with IOC inferred weights
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import numpy as np
import pinocchio as pin
import pybullet as p
import csv
import matplotlib.pyplot as plt
import time
from OCP import OptimalControlProblem
from MPC.simulators.armSimulator import ArmSimulator
from MPC.simulators.targetSimulator import TargetSimulator
import IOC_utils
import utils

#################################################
#------------------ Setup data ------------------
#################################################
rows = [32]      # Rows to be inspected; Leave empty to inspect all trajectories with errors larger than threshold
threshold = 50   # Set to 0 to inspect all
data_file = 'setup5.csv' 

# Plots flags
do_plot_results = False       # Comparison between original and recreated eef trajectories
do_plot_state_results = True # Comparison between original and recreated states
do_plot_controls = True
do_show_simulation = False

# Create robot arm
DT = utils.DT
T = utils.T
robot_urdf = utils.robot_urdf
mesh_dir = utils.mesh_dir
end_effector = utils.end_effector
model, _, _  = pin.buildModelsFromUrdf(robot_urdf, mesh_dir)
mdata = model.createData()

# Setup simulation
if do_show_simulation: 
    pybDT = utils.pybDT
    p.connect(p.GUI)
    p.setGravity(0, 0, -9.81)
    p.setPhysicsEngineParameter(fixedTimeStep=pybDT, numSubSteps=1)
    target_urdf = utils.target_urdf
    joint_names = utils.joint_names
    simulator = ArmSimulator(DT, robot_urdf, mesh_dir, joint_names, pybDT)
    target_sim = TargetSimulator(np.array([0.0,0.0,-0.55]), target_urdf)

# Open csv file
with open(data_file, 'r') as results_file: # Open file in read mode
    csv_reader = csv.reader(results_file)  # Load csv data
    data = np.array(list(csv_reader)) 

#################################################
#------------------ Solve OCP -------------------
#################################################
# Separate data with big errors
if not rows:                            # If the list is empty
    for i in range(1, data.shape[0]-1): # Go through all rows
        if float(data[i, 11]) >= threshold:  # If max error is larger than the threshold
            rows.append(i)              # Add this error to the list of rows to be inspected
else: 
    rows = [a-1 for a in rows]          # Because csv counts from 1, and lists count from 0

for row in rows:                        # Go through each row marked for inspection
    # Get data from relevant columns
    cluster_folder = data[row, 0]
    mid =  [e for e in data[row,1].split(' ')]
    target_folder = mid[0]
    shift_folder = mid[1]
    x0 = utils.get_element(data, row, 15)
    target = utils.get_element(data, row, 3)
    w = utils.get_element(data, row, 8) 

    # Load optimal trajectory
    running_w, terminal_w = utils.create_w(w)
    ocp = OptimalControlProblem(model, mdata, DT, T, end_effector)
    folder_name = 'MPC/' + cluster_folder + '/' + target_folder + '/' + shift_folder    # Open folder for each target 
    trajectory_opt = np.loadtxt(folder_name+'/traj_OCP.txt', comments="#", delimiter=" ", unpack=False)
    eef_opt = ocp.get_eef(trajectory_opt)

    # Run OCP
    trajectory_new, control_new, _ = ocp.run(x0, target, running_w, terminal_w, do_print=False , do_print_logs=False)
    eef_new = ocp.get_eef(trajectory_new)

    # Show simulation
    if do_show_simulation:
        target_sim.change(target)
        for n in range(T):
            # simulator.reset(trajectory_opt[n,:]) # Uncomment to see original (observed) trajectory
            simulator.reset(trajectory_new[n,:])  
            time.sleep(1./100.)

    # Plot 
    if do_plot_results:
        IOC_utils.plot_eef_trajectories(eef_opt, eef_new, "Results IOC: "+cluster_folder + target_folder, DT)
    if do_plot_state_results:
        IOC_utils.plot_joint_trajectories(trajectory_opt, trajectory_new, "Results IOC: "+cluster_folder + target_folder, DT)
    if do_plot_controls:
        IOC_utils.plot_controls(trajectory_opt, trajectory_new, "Results IOC: "+cluster_folder + target_folder, DT)

if do_show_simulation:
    target_sim.remove()
    simulator.remove()
plt.show()