### Functions for derivative-free IOC
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) # Add parent folder to path

from numpy import sum
from numpy.linalg import norm
from scipy.optimize import minimize
import utils
import MPC.plots as plots

# Initialize lists to store intermediate resutls
iterations_w = []
iterations_objective_function=[]
trajs_iterations = []

def callback_fun(w, state): 
    """Function called after every iteration of optimizer.
    Saves intermediate results."""
    global trajectory_eef
    iterations_w.append(w)
    iterations_objective_function.append(state.fun)
    trajs_iterations.append(trajectory_eef)

def fmin(w, ocp, target, x0, eef_opt, norm_ord):
    """Function to be minimized with IOC optimization. 
    Objective function: Euclidean distance between 2 end-effector trajectories
        sum{i in [0,T]} ||x_i-x*_i||^2 for current guess and reference trajs"""
    global trajectory_eef

    running_w, terminal_w = utils.create_w(w)

    if all([x >= 0 for x in running_w]) and all([x >= 0 for x in terminal_w]):
        trajectory_new, _, _ = ocp.run(x0, target, running_w, terminal_w, do_print_logs=False)
        trajectory_eef  = ocp.get_eef(trajectory_new)
        distance = sum(norm(eef_opt-trajectory_eef, axis=1, ord=norm_ord)) 
        return distance
    else:
        # Prevent exploring negative weights
        return(1000)

def run_IOC(w_init, ocp, eef_opt, target, x0, bounds, do_plot_iterations=False, norm_ord=2):
    """Run derivative free IOC - minimize distance between eef trajectories: min ||FK(OPC(w))-x*||^2
    
    ----Parameters----
    w_init  : initial guess for the weights
    ocp     : Optimal control problem (contains model, eef, DT, T - variables for crocoddyl)
    eef_opt : end effector trajectory in cartesian coordnates
    target  : [x,y,z]
    x0      : initial state [q, v]
    bounds  : for w
    do_plot_iterations : flag
    norm_ord: norm of distance between eef used in objective function. Default is 2 i.e. Euclidean distance
    
    ----Returns-----
    w : object containing new weights
    trajs_iterations : intermediate trajectories"""
    w = minimize(fmin, w_init, 
            args = (ocp, target, x0, eef_opt, norm_ord),
            method='trust-constr',    
            callback = callback_fun,  # Uncomment to plot iterations!!!
            bounds=bounds, 
            options={'disp': True, 'maxiter':5000, 'verbose':0}) # Change verbose 2 to print steps
    print("--------------------------")
    print(w) # Print optimization results
    if do_plot_iterations:
        plots.iteration_steps(iterations_w, iterations_objective_function)
    return w, trajs_iterations