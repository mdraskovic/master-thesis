### Run derivative-free IOC for all trajectories (including augmented)
### No parallel running because each solution is warm-started from the previous one  
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import numpy as np
import pinocchio as pin
from scipy.optimize import Bounds 
from OCP import OptimalControlProblem
import pandas as pd
import derivative_free as IOC
import IOC_utils
import utils

#################################################
#------------------ Setup data ------------------
#################################################
cluster_folder = IOC_utils.cluster_folder
target_number = IOC_utils.target_number
shift_number = IOC_utils.shift_number
save_file = IOC_utils.save_file

# Initial guess of weights
w_init = np.array([1, 1, 0.1, 1])  
w = w_init.copy()

# Create robot arm
DT = utils.DT
T = utils.T
robot_urdf = utils.robot_urdf
mesh_dir = utils.mesh_dir
end_effector = utils.end_effector
model, _, _  = pin.buildModelsFromUrdf(robot_urdf, mesh_dir)
data = model.createData()

# Define fixed IOC variables
ocp = OptimalControlProblem(model, data, DT, T, end_effector)
bnds_l = [0.0001 for _ in w_init]
bnds_u = [100 for _ in w_init]
bounds = Bounds(bnds_l, bnds_u)

#################################################
#----------- Function that calls IOC ------------
#################################################
def work(work_data):
    """Function that calls one IOC"""
    target = work_data[0]
    x0 = work_data[1]
    eef_opt = work_data[2] 
    w = work_data[3]
    bounds = work_data[4]
    j = work_data[5]
    i = work_data[6]
    n = work_data[7]
    ocp = OptimalControlProblem(model, data, DT, T, end_effector)

    ret, _ = IOC.run_IOC(w, ocp, eef_opt, target, x0, bounds, False, 2) 
    w = ret.x

    # Save results
    running_w, terminal_w = utils.create_w(w)
    trajectory_new, _ , _= ocp.run(x0, target, running_w, terminal_w)
    eef_new = ocp.get_eef(trajectory_new)   

    err_norm2 = max(np.linalg.norm(eef_opt-eef_new, axis=1, ord=2))*100 # Max distance in 3D space 
    err1 = max(abs(eef_opt-eef_new)[:,0])*100 # Error in x axis in cm
    err2 = max(abs(eef_opt-eef_new)[:,1])*100 # y
    err3 = max(abs(eef_opt-eef_new)[:,2])*100 # z
    results = [j, 'target'+str(i)+' shift'+str(n), eef_opt[0,:], target, running_w, terminal_w, 
            ret.fun, ret.execution_time, w, ret.grad, ret.lagrangian_grad, 
            err_norm2, err1, err2, err3, x0] 
    
    df = pd.DataFrame(columns = {'CLUSTER', 'TARGET', 'START EEF POS', 'END EEF POS',
                    'RUNNING W', 'TERMINAL W', 'OBJ FUN', 'EXECUTION TIME',
                    'W', 'GRAD', 'LAGRANGIAN GRAD', 'MAX ERROR', 'ERROR X' ,
                    'ERROR Y', 'ERROR Z', 'X0 INITIAL STATE'})
    df = df[['CLUSTER', 'TARGET', 'START EEF POS', 'END EEF POS',  # Rearranges column names in this order 
                    'RUNNING W', 'TERMINAL W', 'OBJ FUN', 'EXECUTION TIME',
                    'W', 'GRAD', 'LAGRANGIAN GRAD', 'MAX ERROR', 'ERROR X' ,
                    'ERROR Y', 'ERROR Z', 'X0 INITIAL STATE']]

    df.loc[len(df.index)] = results
    df.to_csv(save_file, mode='a', header=not os.path.exists(save_file), index=False)

    return w

#################################################
#------------- Go through all data --------------
#################################################
for j in cluster_folder:    # j : Go through all clusters
    for i in target_number: # i : Go through all targets in a cluster

        # Get init and target eef position
        ts = np.loadtxt('MPC/'+ j + '/target' + str(i) + '/target_sequence.txt', comments="#", delimiter=" ", unpack=False, dtype = np.float32)
        x0s = np.loadtxt('MPC/'+ j + '/target' + str(i) + '/x0_sequence.txt', comments="#", delimiter=" ", unpack=False, dtype = np.float32)

        w = w_init.copy() # Reset initialization for a new trajectory

        for n in shift_number: # n : Go through each trajectory point by point
            # Get reference trajectories
            folder_name = 'MPC/'+ j + '/target'+str(i)+'/shift'+str(n)     # Open folder for each target 
            trajectory_opt = np.loadtxt(folder_name+'/traj_OCP.txt', comments="#", delimiter=" ", unpack=False)
            eef_opt = ocp.get_eef(trajectory_opt)

            #################################################
            #------------------run IOC ----------------------
            #################################################
            work_data = [ts[n], x0s[n], eef_opt, w, bounds, j, i, n] # Create data tuple
            w = work(work_data) # Keep w from previous IOC for warmstart for augmented trajectories