### Plot OCP weights for original and augmented trajectories corresponding to 1 reaching motion 
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import csv
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.rcParams['axes.grid'] = True
import utils

font = {'size'   : 16} # Increase size of figure labes
matplotlib.rc('font', **font)

#################################################
#------------------ Setup data ------------------
#################################################
save_file = 'setup5.csv'
rows = range(3023,3053)  # first row where it appears -> last row+1
# Plots in the report produced with:
    # 2 -> 32; 
    # 631 -> 661
    # 3023 -> 3053
    
# Open csv file
with open(save_file, 'r') as results_file: # Open file in read mode
    csv_reader = csv.reader(results_file)  # Load csv data
    data = np.array(list(csv_reader)) 
data = data[1:,:]                          # Remove header

#################################################
#-------------------- Plots ---------------------
#################################################
# Setup plots
fig, a = plt.subplots(1,4, figsize=(18,6)) # 2D plots
t = 0

# Load and plot all weights
for row in rows:                 # Go through all rows
    # Get weights
    w = utils.get_element(data, row, 8)    

    # 2D PLOT
    ttl = ['Frame translation cost', 'State regularization cost', 'Joint acceleration cost', 'Terminal frame cost']
    for i in [0,1,2]:
        a[i].scatter(t, w[i], color='black')
        a[i].set(xlabel='Augmented trajectories', title = ttl[i])
    t += 1

    a[3].scatter(t, "{:.2f}".format(w[3]), color='black') 
    a[3].set(xlabel='Augmented trajectories', title = ttl[3])

a[0].set(ylabel=r"Weights value")
# fig.suptitle('OCP Weights Along the Augmented Trajectories')
plt.show()