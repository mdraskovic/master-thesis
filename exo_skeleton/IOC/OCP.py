### OCP for a reaching task solved with a (F)DDP algorithm from Crocoddyl library
### Tailored for IOC
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) # Add parent folder to path

import numpy as np
import crocoddyl
import pinocchio as pin
import matplotlib.pyplot as plt
plt.rcParams['axes.grid'] = True
from matplotlib.patches import Patch
import random; random.seed(4)
import MPC.ddp.weights as weights
import utils

class OptimalControlProblem:
    """    
    ----Attributes----
    model, data : from Pinocchio instance
    end_effector : last link in the system -> "hand"
    DT, T        : OCP timestep and horizon
    
    ----Methods------
    create_models()
        Setup the OCP running and terminal models for Crocoddyl
    inverse_kinematics()
        Get joint angles based on the end-effector position
    forward_kinematics()
        Get end-effector position based on the joint angles
    jacobian()
        Derivative of forward kinematics
    pinv()
        Right pseudo-inverse
    run()
        Solve a DDP algorithm 
    get_eef()
        Forward kineamtics for the whole trajectory
    save_trajectories()
        Save optimal trajectories in folder used later for IOC. Create suboptimal trajecotries for old version of IOC

    """
    def __init__(self, model, data, DT, T, end_effector):
        self.model = model
        self.data = data
        self.DT = DT
        self.T = T
        self.end_effector = end_effector
        nq = model.nq

        ### Define costs that never change ###
        x_ref, _, _, uReg_w, xReg_w, _ = weights.load(nq, nq)
        state = crocoddyl.StateMultibody(model)
        self.state = state
        self.actuation = crocoddyl.ActuationModelFull(state)
        nu = self.actuation.nu

        # State regularization cost 
        xRegResidual = crocoddyl.ResidualModelState(state, x_ref, nu)
        xActivation = crocoddyl.ActivationModelWeightedQuad(xReg_w)
        self.xRegCost = crocoddyl.CostModelResidual(state, xActivation, xRegResidual)
        # Control regularization cost (Warmstart with compensation of gravity)
        uRegResidual = crocoddyl.ResidualModelControlGrav(state, nu)
        uActivation = crocoddyl.ActivationModelWeightedQuad(uReg_w)
        self.uRegCost = crocoddyl.CostModelResidual(state, uActivation, uRegResidual)     
        # State limits penalizations
        xLimitResidual = crocoddyl.ResidualModelState(state, np.zeros(nq+nq), nu)
        xLimitActivation = crocoddyl.ActivationModelQuadraticBarrier(crocoddyl.ActivationBounds(state.lb, state.ub))
        self.xLimitCost = crocoddyl.CostModelResidual(state, xLimitActivation, xLimitResidual)
        # Control limits penalizations
        umax = np.array([150,70,100,75,0]) 
        umin = np.array([-150,-200,-170,-125,0]) 
        uLimitResidual = crocoddyl.ResidualModelControl(state, np.zeros(nq))
        uLimitActivation = crocoddyl.ActivationModelQuadraticBarrier(crocoddyl.ActivationBounds(umin, umax))
        self.uLimitCost = crocoddyl.CostModelResidual(state, uLimitActivation, uLimitResidual)
        # Energy cost
        energyRes = utils.energyResidual(state, nu)
        energyActivation = crocoddyl.ActivationModelSmooth1Norm(nu)
        self.energyCost = crocoddyl.CostModelResidual(state, energyActivation, energyRes)
        # Angle acceleration
        accelerationReg = utils.jointAccelerationResidual(state, nu, model, data)
        accelerationActivation = crocoddyl.ActivationModelWeightedQuad(uReg_w)
        self.accelerationCost = crocoddyl.CostModelResidual(state, accelerationActivation, accelerationReg)

    def create_models(self, target, running_w, terminal_w, frame_w=np.array([1.]*3)):
        """Setup the OCP running and terminal models for Crocoddyl. Uses costs stored in the class.
        
        ----Parameters----
        target     : [x,y,z]
        running_w  : [frameTranslation, xReg, uReg, xLim, uLim, energy, angleAcceleration]
        terminal_w : [frameTranslation, xReg, xLim]
        frame_w    : frame translation activation weights. Default is all ones.
        
        ----Returns------
        running_model, terminal_model"""
        state = self.state
        actuation = self.actuation
        model = self.model
        nq = model.nq

        # Frame translation costs
        frameResidual = crocoddyl.ResidualModelFrameTranslation(state, model.getFrameId(self.end_effector), target)
        frameActivation = crocoddyl.ActivationModelWeightedQuad(frame_w) 
        frameCost = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)
        
        # Create cost model sums
        runningCostModel = crocoddyl.CostModelSum(state)
        terminalCostModel = crocoddyl.CostModelSum(state)

        # Add costs
        runningCostModel.addCost("frameCost", frameCost, running_w[0]) 
        runningCostModel.addCost("xRegCost", self.xRegCost, running_w[1]) 
        runningCostModel.addCost("uRegCost", self.uRegCost, running_w[2]) 
        runningCostModel.addCost("xLimitCost", self.xLimitCost, running_w[3]) 
        runningCostModel.addCost("uLimitCost", self.uLimitCost, running_w[4])
        runningCostModel.addCost("accelerationCost", self.accelerationCost, running_w[6]) 

        terminalCostModel.addCost("frameCost", frameCost, terminal_w[0]) 
        terminalCostModel.addCost("xRegCost", self.xRegCost, terminal_w[1]) 
        terminalCostModel.addCost("xLimitCost", self.xLimitCost, terminal_w[2]) 

        # Create action models
        running_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel)
        terminal_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, terminalCostModel)
        running_model = crocoddyl.IntegratedActionModelEuler(running_DAM, self.DT)
        terminal_model = crocoddyl.IntegratedActionModelEuler(terminal_DAM)

        # Add armature to take into account actuator's inertia
        running_model.differential.armature = np.array([0.1]*nq)
        terminal_model.differential.armature = np.array([0.1]*nq)
        return running_model, terminal_model

    def inverse_kinematics(self, p_des):
        """Get joint angles (q) based on the end-effector position (p_des)"""
        frame_id = self.model.getFrameId(self.end_effector)
        
        q  = np.array([0.0, -0.261799, 0.0, -0.523599, 0.0]) # Initial guess
        eps    = 0.01
        max_it = 1000
        alpha = 0.1
        
        for i in range(max_it):
            p = self.forward_kinematics( q, frame_id)
            err = (p - p_des)
            if np.linalg.norm(err) <= eps:
                break
            J = self.jacobian( q, frame_id)
            # q = q - alpha * self.pinv(J) @ err # Another way to do it
            v = - alpha * self.pinv(J) @ err  
            q = pin.integrate(self.model, q, v * self.DT)

        q = np.concatenate((q,v))  
        return q

    def forward_kinematics(self, q, frame_id):
        """Get the end-effector translation [x,y,z] of the link with id frame_id, based on the joint angles (q) """
        pin.forwardKinematics(self.model, self.data, q)
        pose = pin.updateFramePlacement(self.model, self.data, frame_id)
        return pose.translation

    def jacobian(self, q, frame_id):
        """Calculate the derivative of the forward kinematics function of a frame woth frame_id, at a given configuration q"""
        pin.framesForwardKinematics(self.model, self.data, q)
        pose = pin.updateFramePlacement(self.model, self.data, frame_id)
        body_jacobian = pin.computeFrameJacobian(self.model, self.data, q, frame_id)
        Ad = np.zeros((6, 6))
        Ad[:3, :3] = pose.rotation
        Ad[3:, 3:] = pose.rotation
        J = Ad @ body_jacobian
        Jlin = J[:3, :]
        return Jlin

    def pinv(self, J, eps=1e-3):
        """Calculate the right pseudoinverse of a matrix J"""
        JJT = J @ J.T
        return J.T @ np.linalg.inv((JJT + eps * np.eye(*JJT.shape)))

    def run(self, x0, target, running_w, terminal_w, frame_w=np.array([1.]*3), do_print=False, do_print_logs=False):
        """Solve a DDP using Crocoddyl and print results.
        
        ----Parameters----
        x0         : Initial state [q, q']
        target     : [x,y,z]
        running_w  : [frameTranslation, xReg, uReg, xLim, uLim, energy, angleAcceleration]
        terminal_w : [frameTranslation, xReg, xLim]
        frame_w    : frame translation activation weights. Default is all ones.
        do_print   : flag for printing
        
        ----Returns------
        trajectory : shape T+1 x [q, q']
        control    : torque; shape T x u"""
        running_model, terminal_model = self.create_models(target, running_w, terminal_w, frame_w)
        problem = crocoddyl.ShootingProblem(x0, [running_model]*self.T, terminal_model)
        ddp = crocoddyl.SolverFDDP(problem)
        if do_print_logs:
            ddp.setCallbacks([crocoddyl.CallbackLogger(), crocoddyl.CallbackVerbose()])
        
        # Warm start
        # x0_cpy = x0.copy() # Use initial state as initial guess
        # x0_cpy[5:] = 0
        # xs_init = [x0_cpy for i in range(self.T+1)]   

        target_state = self.inverse_kinematics(target) # Use target as initial guess
        xs_init = [target_state for i in range(self.T+1)]

        # xs_init , us_init = self.warmstart_interpolation( x0, target)
        us_init = ddp.problem.quasiStatic(xs_init[:-1])  

        tf = ddp.solve(xs_init, us_init, maxiter=100, isFeasible=False)
        trajectory = np.array(ddp.xs)
        control = np.array(ddp.us)

        if do_print_logs:
            log = ddp.getCallbacks()[0]
            crocoddyl.plotOCSolution(log.xs, log.us, figIndex=1, show=False)
            crocoddyl.plotConvergence(log.costs, log.u_regs, log.x_regs, log.grads, log.stops, log.steps, figIndex=2)
        if do_print:
            model = self.model
            data = self.data
            pin.forwardKinematics(model, data, trajectory[-1][0:5], trajectory[-1][5:]) # Fwd kinematics to get final eef
            pin.updateFramePlacements(model, data)

            print("----------------------")
            print("Target is ", target)
            print("Problem solved:", tf)
            print("Number of iterations:", ddp.iter)
            print("Total cost:", ddp.cost)
            print("Gradient norm:", ddp.stoppingCriteria())
            print("Finally reached: ", np.array(data.oMf[model.getFrameId(self.end_effector)].translation.T.flat))
        return trajectory, control, tf

    def get_eef(self, trajectory):
        """Forward kinematics for the whole trajectory.
        Get trajectory of eef (shape T x [x,y,z]) from a joint trajectory."""
        shape = trajectory.shape[0]
        eef = np.zeros((shape,3))
        model = self.model
        data = self.data
        frame_id = model.getFrameId(self.end_effector)
        for i in range(shape):
            pin.forwardKinematics(model, data, trajectory[i][0:5], trajectory[i][5:])
            pin.updateFramePlacements(model, data)
            eef[i][:] = np.array(data.oMf[frame_id].translation.T.flat)
        return eef

    def save_trajectories(self, x0, target, trajectory, control, folder_name, number_suboptimal=0, do_plot_traj=False, do_plot_states=False):
        """Save optimal trajectory in folder used later for IOC.
        Create suboptimal trajectories by setting a random via point in space. This is used for old verison of IOC.
        Save suboptimal trajectories.
        
        ----Parameters----
        x0         : Initial state [q, v]
        target     : [x,y,z]
        trajectory, control : Optimal values to be saved
        folder_name : Specified as 'targetX' for certain target number X
        number_suboptimal : Number of suboptimal trajectories to be created and saved. Not used for derivative-free IOC. Default 0
        do_plot_traj, do_plot_states: Flags for plotting
        
        ----Returns------
        trajectory : shape T+1 x [q, q']
        control    : torque; shape T x u
        """
        np.savetxt(folder_name +'/traj_OCP.txt', trajectory)
        np.savetxt(folder_name +'/ctrl_OCP.txt', control)

        ###################################################
        #---- Create and save suboptimal trajectories ----#
        ###################################################
        # Load variables from class (for speed)
        model = self.model
        data = self.data
        T = self.T
        nq = model.nq
        frame_id = model.getFrameId(self.end_effector)
        _, running_w, terminal_w, _, _, frame_w = weights.load(nq, nq)
        state = self.state
        actuation = self.actuation
        xRegCost = self.xRegCost
        uRegCost = self.uRegCost
        xLimitCost = self.xLimitCost
        uLimitCost = self.uLimitCost
        frameActivation = crocoddyl.ActivationModelWeightedQuad(frame_w) 

        # Setup plots
        if do_plot_states:
            fig1, axs1 = plt.subplots(2, 2)
            fig2, axs2 = plt.subplots(2, 2)
            fig3, axs3 = plt.subplots(2, 2)
            fig1.suptitle("Optimal and Suboptimal: Joint Position")
            fig2.suptitle("Optimal and Suboptimal: Joint Velocity")
            fig3.suptitle("Optimal and Suboptimal: Torque")
        if do_plot_traj:
            fig4 = plt.figure() 
            ax4 = fig4.add_subplot(111, projection = '3d')
            ax4.set_xlabel('x [cm]'), ax4.set_ylabel('y [cm]'), ax4.set_zlabel('z [cm]')
            # fig4.suptitle("Optimal and Suboptimal Trajectories")
        rad_to_deg = 180.0/3.141592653589793
        timesteps = np.arange(0, control.shape[0], 1)
        joint_names = ["Shoulder Roll","Shoulder Pitch","Shoulder Yaw","Elbow Pitch",""]
        ccc_red=['xkcd:coral', 'xkcd:coral pink', 'xkcd:salmon', 'xkcd:pale red', 'xkcd:light red']

        for i in range(number_suboptimal):
            ### Find random via point ###
            pin.forwardKinematics(model, data, trajectory[0][0:5], trajectory[0][5:])
            pin.updateFramePlacements(model, data)
            begin_position = np.array(data.oMf[frame_id].translation.T.flat)
            via_point = (begin_position + target)/2.0 

            if i!=0:  # First via point is just between start & stop => straight line as a subopt traj
                via_point[0] += ((random.randint(0,1)*2)-1)*random.uniform(0.05, 0.15)
                via_point[1] += ((random.randint(0,1)*2)-1)*random.uniform(0.05, 0.15)
                via_point[2] += ((random.randint(0,1)*2)-1)*random.uniform(0.05, 0.15)

            ### Create and solve an OCP ###
            # Frame cost for via point
            frameResidual = crocoddyl.ResidualModelFrameTranslation(state, frame_id, via_point)
            frameCost = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)
            runningCostModel = crocoddyl.CostModelSum(state)
            runningCostModel.addCost("frameCost", frameCost, running_w[0]*100) 
            runningCostModel.addCost("xRegCost", xRegCost, running_w[1]) 
            runningCostModel.addCost("uRegCost", uRegCost, running_w[2]) 
            runningCostModel.addCost("xLimitCost", xLimitCost, running_w[3]) 
            runningCostModel.addCost("uLimitCost", uLimitCost, running_w[4]) 
            running_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel)
            running_model = crocoddyl.IntegratedActionModelEuler(running_DAM, self.DT)
            running_model.differential.armature = np.array([.1]*nq)
            running_models_via_point = [running_model for j in range(int(T/2))] 
            # Frame cost for target
            frameResidual = crocoddyl.ResidualModelFrameTranslation(state, frame_id, target)
            frameCost = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)
            runningCostModel = crocoddyl.CostModelSum(state)
            runningCostModel.addCost("frameCost", frameCost, running_w[0]*100) 
            runningCostModel.addCost("xRegCost", xRegCost, running_w[1]) 
            runningCostModel.addCost("uRegCost", uRegCost, running_w[2]) 
            runningCostModel.addCost("xLimitCost", xLimitCost, running_w[3]) 
            runningCostModel.addCost("uLimitCost", uLimitCost, running_w[4]) 
            running_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel)
            running_model = crocoddyl.IntegratedActionModelEuler(running_DAM, self.DT)
            running_model.differential.armature = np.array([.1]*nq)
            running_models_target = [running_model for j in range(int(T/2))] 
            
            running_models = running_models_via_point + running_models_target

            # Create terminal model
            frameResidual = crocoddyl.ResidualModelFrameTranslation(state, frame_id, target)
            frameCost = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)
            terminalCostModel = crocoddyl.CostModelSum(state)
            terminalCostModel.addCost("frameCost", frameCost, terminal_w[0]) 
            terminalCostModel.addCost("xRegCost", xRegCost, terminal_w[1]) 
            terminalCostModel.addCost("xLimitCost", xLimitCost, terminal_w[2]) 
            terminal_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, terminalCostModel)
            terminal_model = crocoddyl.IntegratedActionModelEuler(terminal_DAM)
            terminal_model.differential.armature = np.array([.1]*nq)

            # Create and solve shooting problem
            problem = crocoddyl.ShootingProblem(x0, running_models, terminal_model)
            ddp = crocoddyl.SolverDDP(problem)
            tf = ddp.solve()

            ### Save suboptimal trajectories ###
            non_opt_traj = np.array(ddp.xs)
            non_opt_ctrl = np.array(ddp.us)
            np.savetxt(folder_name +'/ctrl_OCP' + str(i) + '.txt', non_opt_ctrl)
            np.savetxt(folder_name +'/traj_OCP' + str(i) + '.txt', non_opt_traj)

            ###################################################
            #--------------------- Plots ---------------------#
            ###################################################
            if do_plot_states: # Plot joint state
                ctr = -1
                for n in [0,1]:
                    for m in [0,1]:
                        ctr += 1
                        axs1[n,m].plot(timesteps, rad_to_deg*non_opt_traj[:-1,ctr], linewidth=1.0, label = "suboptimal", color=ccc_red[i%len(ccc_red)], alpha=0.8)
                        axs2[n,m].plot(timesteps, rad_to_deg*non_opt_traj[:-1,5+ctr], linewidth=1.0, label = "suboptimal", color=ccc_red[i%len(ccc_red)], alpha=0.8)
                        axs3[n,m].plot(timesteps, non_opt_ctrl[:,ctr], linewidth=1.0, label = "suboptimal", color=ccc_red[i%len(ccc_red)], alpha=0.8)
            if do_plot_traj: # Plot 3D eef trajectory
                eef2 = self.get_eef(non_opt_traj).T
                ax4.plot(eef2[0]*100, eef2[1]*100, eef2[2]*100, color=ccc_red[i%len(ccc_red)], linewidth=1.0)
                ax4.scatter(*eef2.T[0]*100, edgecolor = 'red', marker='D', c='none') 

        # Plot optimal trajectory
        pa1 = Patch(facecolor='xkcd:coral'); pa2 = Patch(facecolor='xkcd:coral pink'); pa3 = Patch(facecolor='xkcd:salmon'); pb = Patch(facecolor='xkcd:emerald green')
        handles = [pa1, pb, pa2, pb,  pa3, pb]; labels=['', '', '', '','Suboptimal', 'Optimal']
        if do_plot_states:
            ctr = -1
            for n in [0,1]:
                for m in [0,1]:
                    ctr += 1
                    axs1[n,m].plot(timesteps, rad_to_deg*trajectory[:-1,ctr], linewidth=1.0, color='xkcd:emerald green')
                    axs1[n,m].set(xlabel='Time [s]', ylabel='Joint position [deg]', title=joint_names[ctr], label = "optimal")
                    axs2[n,m].plot(timesteps, rad_to_deg*trajectory[:-1,5+ctr], linewidth=1.0, color='xkcd:emerald green')
                    axs2[n,m].set(xlabel='Time [s]', ylabel='Joint velocity [deg]', title=joint_names[ctr], label = "optimal")
                    axs3[n,m].plot(timesteps, control[:,ctr], linewidth=1.0, color='xkcd:emerald green')
                    axs3[n,m].set(xlabel='Time [s]', ylabel='Torque [Nm]', title=joint_names[ctr], label = "optimal")
            # Create legends
            fig1.legend(handles=handles,labels=labels, ncol=3, handletextpad=0.5, handlelength=1,handleheight=0.3, columnspacing=-0.5)
            fig2.legend(handles=handles,labels=labels, ncol=3, handletextpad=0.5, handlelength=1,handleheight=0.3, columnspacing=-0.5)
            fig3.legend(handles=handles,labels=labels, ncol=3, handletextpad=0.5, handlelength=1,handleheight=0.3, columnspacing=-0.5)
        if do_plot_traj:
            eef1 = self.get_eef(trajectory).T
            ax4.plot(eef1[0]*100, eef1[1]*100, eef1[2]*100, color='xkcd:emerald green', linewidth=1.5)
            ax4.scatter(*eef1.T[0]*100, color = 'xkcd:emerald green') # Mark starting point
            ax4.legend(handles=handles,labels=labels, ncol=3, handletextpad=0.5, handlelength=1,handleheight=0.3, columnspacing=-0.5)