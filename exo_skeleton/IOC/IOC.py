### Class for IOC based on cost functions (Old version of IOC)
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import numpy as np
np.set_printoptions(linewidth=np.inf)
import crocoddyl

class optimization_problem:
    """
    ----Methods------
    create_phi()
        Cretae a matrix containing all costs for all trajectories
    phi()
        Calculate phi vector for one trajectory
    cost_function()
        Calculate cost function J = w*phi
    load_trajectories()
        Load state trajectories from files"""

    def __init__(self):
        pass

    def create_phi(self, ocp, target_sequence, number_suboptimal, dimension, cluster_folder=None):
        """Load variables used in optimization. 
        Create phi matrix containing Crocoddyl costs:
            phi_xminus : Shape /target/ x /number_suboptimal/ x /dimension/ ->>/dimension/ values for all suboptimal trajectories
            phi_xstar  : Shape /target/ x /dimension/   - For the optimal one.
        To create running and terminal costs, just multiply matrix phi with weight vector.

        ----Parameters----
        ocp : Optimal control problem (contains model, eef, DT, T - variables for crocoddyl)
        number_suboptimal : Number of suboptimal trajectories used in objective function 
        dimension : Number of unknown variables
        cluster_folder : Name of the folder with targets cluster
        """
        T = ocp.T  # Load all values for speed
        DT = ocp.DT
        model = ocp.model
        self.target_sequence = target_sequence
        self.number_suboptimal = number_suboptimal
        self.dimension = dimension 
        len_target_seq = len(target_sequence)

        trajs, ctrls = load_trajectories(len_target_seq, number_suboptimal, T, cluster_folder) 
                # eg. number_targets x (numb_subopt+1 trajs) x (T+1 nodes) x 10states 
        self.trajs = trajs
        self.ctrls = ctrls
 
        ### Create matrices phi ###
        state = crocoddyl.StateMultibody(model)
        actuation = crocoddyl.ActuationModelFull(state)

        # Create cost model sums that will be used for each cost individually
        runningCostModel_xReg = crocoddyl.CostModelSum(state)
        runningCostModel_uReg = crocoddyl.CostModelSum(state)
        runningCostModel_xLim = crocoddyl.CostModelSum(state)  
        runningCostModel_uLim = crocoddyl.CostModelSum(state) 
        runningCostModel_energy = crocoddyl.CostModelSum(state) 
        runningCostModel_acc = crocoddyl.CostModelSum(state)  
        terminalCostModel_xReg = crocoddyl.CostModelSum(state)
        terminalCostModel_xLim = crocoddyl.CostModelSum(state) 
        
        # Add costs to matching cost models 
        runningCostModel_xReg.addCost("xRegCost", ocp.xRegCost, 1) 
        runningCostModel_uReg.addCost("uRegCost", ocp.uRegCost, 1) 
        runningCostModel_xLim.addCost("xLimitCost", ocp.xLimitCost, 1) 
        runningCostModel_uLim.addCost("uLimitCost", ocp.uLimitCost, 1) 
        runningCostModel_energy.addCost("energyCost", ocp.energyCost, 1) 
        runningCostModel_acc.addCost("accelerationCost", ocp.accelerationCost, 1) 
        terminalCostModel_xReg.addCost("xRegCost", ocp.xRegCost, 1) 
        terminalCostModel_xLim.addCost("xLimitCost", ocp.xLimitCost, 1) 
        
        # Create differential action models
        running_DAM_xReg = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel_xReg)
        running_DAM_uReg = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel_uReg)
        running_DAM_xLim = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel_xLim)
        running_DAM_uLim = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel_uLim)
        running_DAM_energy = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel_energy)
        running_DAM_acc = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel_acc)
        terminal_DAM_xReg = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, terminalCostModel_xReg)
        terminal_DAM_xLim = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, terminalCostModel_xLim)
        # Create integrated action models
        self.running_model_xReg = crocoddyl.IntegratedActionModelEuler(running_DAM_xReg, DT)
        self.running_model_uReg = crocoddyl.IntegratedActionModelEuler(running_DAM_uReg, DT)
        self.running_model_xLim = crocoddyl.IntegratedActionModelEuler(running_DAM_xLim, DT)
        self.running_model_uLim = crocoddyl.IntegratedActionModelEuler(running_DAM_uLim, DT)
        self.running_model_energy = crocoddyl.IntegratedActionModelEuler(running_DAM_energy, DT)
        self.running_model_acc = crocoddyl.IntegratedActionModelEuler(running_DAM_acc, DT)
        self.terminal_model_xReg = crocoddyl.IntegratedActionModelEuler(terminal_DAM_xReg)
        self.terminal_model_xLim = crocoddyl.IntegratedActionModelEuler(terminal_DAM_xLim)
        # Create crocoddyl object data that will contain cost
        self.data_r_xReg = self.running_model_xReg.createData()
        self.data_r_uReg = self.running_model_uReg.createData()
        self.data_r_xLim = self.running_model_xLim.createData()
        self.data_r_uLim = self.running_model_uLim.createData()
        self.data_r_energy = self.running_model_energy.createData()
        self.data_r_acc = self.running_model_acc.createData()
        self.data_t_xReg = self.terminal_model_xReg.createData()
        self.data_t_xLim = self.terminal_model_xLim.createData()

        # Frame placement costs individually as they differ for targets 
        running_model_frame = []
        terminal_model_frame = []
        data_r_frame = []
        data_t_frame = []
        frameActivation = crocoddyl.ActivationModelWeightedQuad(np.array([1.]*3))

        for j in range(len_target_seq):
            runningCostModel_frame = crocoddyl.CostModelSum(state)
            terminalCostModel_frame = crocoddyl.CostModelSum(state)

            frameResidual = crocoddyl.ResidualModelFrameTranslation(state, model.getFrameId(ocp.end_effector), target_sequence[j])
            frameCost = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)
            runningCostModel_frame.addCost("frameCost", frameCost, 1)
            terminalCostModel_frame.addCost("frameCost", frameCost, 1)

            running_DAM_frame = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, runningCostModel_frame)
            terminal_DAM_frame = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, terminalCostModel_frame)

            running_model_frame.append(crocoddyl.IntegratedActionModelEuler(running_DAM_frame, DT))
            terminal_model_frame.append(crocoddyl.IntegratedActionModelEuler(terminal_DAM_frame))
            data_r_frame.append(running_model_frame[j].createData())
            data_t_frame.append(terminal_model_frame[j].createData())
        
        self.running_model_frame = running_model_frame
        self.terminal_model_frame = terminal_model_frame
        self.data_r_frame = data_r_frame
        self.data_t_frame = data_t_frame

        # Create matrix phi
        phi_xstar = np.zeros((len_target_seq, dimension))
        phi_xminus = np.zeros((len_target_seq, number_suboptimal, dimension))
        for j in range(len_target_seq):   # Go tru all targets
            phi_xstar[j,:] = self.phi(trajs[j,0,:,:], ctrls[j,0,:,:], T, j)
            for i in range(number_suboptimal):  # Go tru all subotpimal trajectories
                phi_xminus[j,i,:] = self.phi(trajs[j,i+1,:,:], ctrls[j,i+1,:,:], T, j) 
        self.phi_xstar = phi_xstar
        self.phi_xminus = phi_xminus 

    def phi(self, traj, ctrl, T, j):
        """Calculate phi vector for certain trajectory.
        
        ----Parameters----
        traj : Shape 151x10 = (traj length)x(states) = one trajectory containing states (pos and vel for each joint) for all T points
        ctrl : Shape 150x5  = (ctrl length)x(controls)
        T    : number of nodes that DDP uses
        j    : target number, because of frame cost
        
        ----Returns------
        phi : np.array of costs""" 
        cost_r_frame = 0 # Not all these are used in the end
        cost_r_xReg = 0 
        cost_r_uReg = 0
        cost_r_xLim = 0
        cost_r_uLim = 0
        cost_r_energy = 0
        cost_r_acc = 0

        cost_t_frame = 0
        cost_t_xReg = 0
        cost_t_xLim = 0
        
        # Running costs
        for t in range(T):
            self.running_model_frame[j].calc(self.data_r_frame[j], traj[t,:], ctrl[t,:])
            cost_r_frame += self.data_r_frame[j].cost

            self.running_model_xReg.calc(self.data_r_xReg, traj[t,:], ctrl[t,:])
            cost_r_xReg += self.data_r_xReg.cost

            self.running_model_uReg.calc(self.data_r_uReg, traj[t,:], ctrl[t,:])
            cost_r_uReg += self.data_r_uReg.cost

            self.running_model_xLim.calc(self.data_r_xLim, traj[t,:], ctrl[t,:])
            cost_r_xLim += self.data_r_xLim.cost

            self.running_model_uLim.calc(self.data_r_uLim, traj[t,:], ctrl[t,:])
            cost_r_uLim += self.data_r_uLim.cost

            self.running_model_energy.calc(self.data_r_energy, traj[t,:], ctrl[t,:])
            cost_r_energy += self.data_r_energy.cost

            self.running_model_acc.calc(self.data_r_acc, traj[t,:], ctrl[t,:])
            cost_r_acc += self.data_r_acc.cost
        
        # Terminal costs
        self.terminal_model_frame[j].calc(self.data_t_frame[j], traj[T,:])
        cost_t_frame += self.data_t_frame[j].cost

        self.terminal_model_xReg.calc(self.data_t_xReg, traj[T,:])
        cost_t_xReg += self.data_t_xReg.cost

        self.terminal_model_xLim.calc(self.data_t_xLim, traj[T,:])
        cost_t_xLim += self.data_t_xLim.cost

        ret_val = np.array([cost_r_frame,
                            cost_r_xReg,
                            cost_r_acc])
        return ret_val

    def cost_function(self, phi, w):
        """w*phi(x)"""
        return np.matmul(phi, w) 
        
def load_trajectories(number_of_targets, number_nonoptimal = 25, T=150, cluster_folder=None):
    """Load [q,q'] trajectories and control torques from files."""
    trajs = np.zeros((number_of_targets, number_nonoptimal+1, T+1, 10)) # Shape: number_of_targets x (suboptimal + 1) x (T+1) x 10
    ctrls = np.zeros((number_of_targets, number_nonoptimal+1, T, 5))    # Shape number_of_targets x (suboptimal + 1) x T x 5
    for j in range(number_of_targets):
        folder_name = cluster_folder + '/target'+str(j)
        # Load optimal trajs
        TT = np.loadtxt(folder_name+'/traj_OCP.txt', comments="#", delimiter=" ", unpack=False)
        C = np.loadtxt(folder_name+'/ctrl_OCP.txt', comments="#", delimiter=" ", unpack=False)
        trajs[j,0,:,:] = TT
        ctrls[j,0,:,:] = C
        # Load nonoptimal trajs
        for i in range(number_nonoptimal):
            TT = np.loadtxt(folder_name+'/traj_OCP' + str(i) + '.txt', comments="#", delimiter=" ", unpack=False)
            C = np.loadtxt(folder_name+'/ctrl_OCP' + str(i) + '.txt', comments="#", delimiter=" ", unpack=False)
            trajs[j,i+1,:,:] = TT
            ctrls[j,i+1,:,:] = C
    return trajs, ctrls