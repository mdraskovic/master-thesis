## URDF

Contains meshes and description of the robot arm. Change the paths to the meshes in the arm.urdf and exoskeleton.urdf

## Data

Data -> CSVFiles folder contains raw data coming from motion capture system. These are 3d coordinates of translatin and rotation of shoulder, elbow and wrist joints during reaching movements. Each csv file is 1 movement from initial position near the body to the reaching goal. All data is separated into 9 clusters depending on where the reaching goal is:  <br /> 
1 - low right <br /> 
2 - central right <br /> 
3 - up right <br /> 
4 - low middle <br /> 
5 - central middle <br /> 
6 - up middle <br /> 
7 - low left <br /> 
8 - central left <br /> 
9 - up left <br /> 
The rest of 'Data' folder is used to study the recorded data: create plots, implement inverse kinematics, create augmented trajectories <br /> 

Results of this section is stored in Produced files->MPC

## IOC

Runs bi-level optimization for each of the state trajectories created and saved from the 'Data' section.

## ML

Learn weights for the controller. Uses results of the 'IOC' section as a training set <br />
Results of this section is stored in Produced files

## MPC

DDP-based MPC controller. Solves OCP every 5 ms using Crocoddyl library.  <br /> 
MPC controller for 1 target, tracking controller and robustness study to access quality of the controller.
