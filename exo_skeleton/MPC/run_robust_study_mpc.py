### Script for MPC robust study
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import numpy as np
import pybullet as p
from simulators.Controller import Controller
from simulators.armSimulator import ArmSimulator
from simulators.targetSimulator import TargetSimulator
from mpc_robust_study import MPC_ArmExo
import utils

#################################################
#------------------ Setup data ------------------
#################################################
arm_torque = 50 # Percantage of torque this controller gives 
exo_torque = 50

callbacks=False
do_print_DDP = False
do_print_MPC = True
do_plot = True

DT = utils.DT
T = utils.T
arm_urdf = utils.arm_urdf
exo_urdf = utils.exo_urdf
mesh_dir = utils.mesh_dir
end_effector = utils.end_effector
target_urdf = utils.target_urdf
joint_names = utils.joint_names
pybDT = utils.pybDT

# Target desired (Ground truth)
# target1 = np.array([0.4, -0.2, -0.2]) 
target1 = np.array([3.8e-01, -4.7e-01, 6.8e-02])
target2 = np.array([0.4, 0.2, 0.35]) 
target3 = np.array([0.4, 0.2, 0.1]) 
target4 = np.array([0.2, -0.45, 0.2])   
target5 = np.array([0.4,-0.25, -0.0])

target_sequence = [target1, target2, target3, target4, target5]
do_plot  = [True, False, False, False, False, False]

# x_desired + error 
target_error_sequence = []
for i in range(len(target_sequence)):
    noise = np.random.normal(0, 0.0) # To simulate arm having a different target, add deviation here
    target_error_sequence.append(target_sequence[i] + noise)       

# Start simulation
p.connect(p.GUI)
p.setGravity(0, 0, -9.81)
p.setPhysicsEngineParameter(fixedTimeStep=pybDT, numSubSteps=1)
simulator = ArmSimulator(DT, arm_urdf, mesh_dir, joint_names, pybDT)  # CHANGE THIS TO ARM
target_simulator = TargetSimulator(target_sequence[0], target_urdf)

#################################################
#--------------------- MPC ----------------------
#################################################
# Define arm and exo
arm = Controller(arm_urdf, mesh_dir, end_effector, DT, T, torque=arm_torque)
exo = Controller(exo_urdf, mesh_dir, end_effector, DT, T, torque=exo_torque)

# Initialize DDP solver
q0,v0 = simulator.get_state()
x0 = np.concatenate([q0, v0])
arm.reset(q0,v0)
exo.reset(q0,v0)
arm.reaching_task.init_solver(x0, target_sequence[0])
exo.reaching_task.init_solver(x0, target_error_sequence[0])

# Initialize MPC
arm.init_mpc(mpcDT=0.005, T_sim=1.5, maxiter=100, shorten_hor=False, time_var_weights=False)
exo.init_mpc(mpcDT=0.005, T_sim=1.5, maxiter=100, shorten_hor=False, time_var_weights=False)

mpc = MPC_ArmExo(simulator, arm, exo)

for i in range(len(target_sequence)):
    print("New target : ", target_sequence[i], target_error_sequence[i])
    target_simulator.change(target_sequence[i]) # Change do_plot to True for plotting torques, states and MPC plans
    mpc.run_static_target(target_sequence[i], target_error_sequence[i], callbacks, do_print_DDP, do_print_MPC, do_plot=do_plot[i])

while True: # keep window open
    pass
p.disconnect()