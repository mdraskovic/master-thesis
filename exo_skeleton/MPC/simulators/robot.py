## Simulation of the Arm
## Author : Avadesh Meduri
## Date : 27/01/2022


import os
from os.path import dirname, join, abspath
import sys

import numpy as np
import pinocchio as pin

class ExoSkeleton:
    
    def __init__(self, dt, urdf_dir, mesh_dir):
        self.model, self.collision_model, self.visual_model = pin.buildModelsFromUrdf(urdf_dir, mesh_dir)
        self.data = self.model.createData()
        
        self.dt = dt
        self.q = np.zeros(self.model.nq)
        self.v = np.zeros(self.model.nq)
        
    def step(self, tau):
        
        """
        This function integrates the robot for one time step
        Used to simulate
        """
        # Forward dynamics
        a = pin.aba(self.model,self.data, self.q, self.v,tau) 
        # Semi-explicit integration
        self.v += a*self.dt
        self.q = pin.integrate(self.model,self.q,self.v*self.dt)
        
    def reset(self):
        self.q = np.zeros(self.model.nq)
        self.v = np.zeros(self.model.nq)   