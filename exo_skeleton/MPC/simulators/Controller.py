### Class for one instance of the controller of the arm model. 
### To simulate in robustness study separately the Arm and the Exoskeleton controllers, create 2 instances of this class.
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.

import pinocchio as pin
from ddp.reachingTask import ReachingTask

class Controller:
    """
    ----Attributes----
    urdf_path, mesh_path   : path to URDF file and files with meshes used in URDF
    end_effector : last link in the system -> "hand"
    DT, T        : OCP timestep and horizon
    torque       : torque precentage it provides; 0-100%

    ----Methods------
    reset()
        Reset model to initial state
    init_mpc()
        Load values used in MPC"""

    def __init__(self, urdf_path, mesh_path, end_effector, DT, T, torque):
        self.end_effector = end_effector 
        self.DT = DT                     
        self.T = T                      
        self.torque = torque 

        # Create Pinocchio instance
        self.model, _, _ = pin.buildModelsFromUrdf(urdf_path, mesh_path)
        self.data = self.model.createData()
        self.nq = self.model.nq
        self.nv = self.model.nq

        self.reaching_task = ReachingTask(self.model, self.data, end_effector, DT, T)
        
    def reset(self, q0, v0):
        """Reset exo to initial state"""
        pin.forwardKinematics(self.model, self.data, q0,v0)
        pin.updateFramePlacements(self.model, self.data)

    def init_mpc(self, mpcDT, T_sim, maxiter, shorten_hor, time_var_weights):
        """Load values used in mpc.
        
        ----Parameters----
        mpcDT        : timestep for replanning
        T_sim        : total length of simulation
        maxiter      : maximal iterations in DDP
        shorten_hor  : indicates the horizon should be shortened to prevent MPC think it has time - Not used in final version
        time_var     : indicates that frame translation cost has increasing cost with a tanh function - Not used in final version"""
        self.mpcDT = mpcDT
        self.T_sim = T_sim
        self.maxiter = maxiter
        self.shorten_hor = shorten_hor
        self.time_var_weights = time_var_weights