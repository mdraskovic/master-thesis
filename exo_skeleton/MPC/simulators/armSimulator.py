### Class for simulating arm model (arm with exoskeleton) in PyBullet
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.

import numpy as np
import pybullet as p
import pinocchio as pin
from scipy.ndimage import zoom

class ArmSimulator:
    """
    ----Attributes----
    DT          : OCP timestep
    urdf_path, mesh_path   : path to URDF file and files with meshes used in URDF
    joint_names : list of all joint names as listed in URDF file
    pyBulletDT  : simulator timestep

    ----Methods------
    get_state()
        Returns position and velocity
    send_joint_command():
        Apply the torques to the joints
    step()
        Step simulation
    reset()
        Reset robot arm to initial state
    interpolate_control()
        Match OCP and simulator timesteps
    match_models()
        Update Pinnochio model to match the pyBullet model
    remove()
        Remove the target from simulation window"""

    def __init__(self, DT, urdf_path, mesh_path, joint_names, pyBulletDT=0.001):
        self.joint_names = joint_names
        self.pyBulletDT = pyBulletDT
        self.interpolation_steps = round(DT/pyBulletDT) # Ratio between OCP and simulation timesteps
        number_joints = self.number_joints = len(joint_names) 

        # Create Pinocchio instance
        self.model, _, _ = pin.buildModelsFromUrdf(urdf_path, mesh_path)  # Other outputs are collision and visual models
        self.data = self.model.createData()
        self.nq = self.model.nq
        self.nv = self.model.nv

        # Load URDF
        basePosition = [0, 0, 0]
        baseOrientation = p.getQuaternionFromEuler([0, 0, 0])
        self.robotId = p.loadURDF(urdf_path, basePosition, baseOrientation, flags=p.URDF_USE_INERTIA_FROM_FILE, useFixedBase=True)

        # Setup torque control (and disable velocity control)
        bullet_joint_map = {}
        for ji in range(p.getNumJoints(self.robotId)):
            bullet_joint_map[p.getJointInfo(self.robotId, ji)[1].decode("UTF-8")] = ji
        
        self.bullet_joint_ids = np.array([bullet_joint_map[name] for name in joint_names]) #[0 1 2 3 4]
        self.pinocchio_joint_ids = np.array([self.model.getJointId(name) for name in joint_names]) # [1 2 3 4 5]

        self.pin2bullet_joint_only_array = []  #[0 1 2 3 4]
        for i in range(1, number_joints+1):
            self.pin2bullet_joint_only_array.append(np.where(self.pinocchio_joint_ids==i)[0][0])

        p.setJointMotorControlArray(self.robotId, self.bullet_joint_ids, 
                        p.VELOCITY_CONTROL, forces=np.zeros(number_joints))

        # Setup initial state
        self.reset()  

    def get_state(self):
        """Returns position and velocity"""
        q = np.zeros(self.nq)
        v = np.zeros(self.nv)
        joint_states = p.getJointStates(self.robotId, self.bullet_joint_ids)
        for i in range(len(self.joint_names)):
            q[self.pinocchio_joint_ids[i] - 1] = joint_states[i][0]
            v[self.pinocchio_joint_ids[i] - 1] = joint_states[i][1]
        return q,v

    def send_joint_command(self, tau):
        """Apply the desired torques (tau) to the joints."""
        if tau.shape[0] != self.nv: # Check if the torque has the correct shape
            return

        zeroGains = tau.shape[0] * (0.0,)

        p.setJointMotorControlArray(self.robotId, self.bullet_joint_ids,
            p.TORQUE_CONTROL, forces=tau[self.pin2bullet_joint_only_array],
            positionGains=zeroGains, velocityGains=zeroGains)
    
    def step(self):
        """Step simulation"""
        p.stepSimulation()

    def reset(self, q=np.array([0.0, -0.261799, 0.0, -0.523599, 0.0]), v=np.zeros(5)):
        """Reset robot arm to initial state x = [q,v] = [joint angles, joint velocities]. 
        Default values are position of arm next to the body and zero joints velocities."""
        for i, bullet_joint_id in enumerate(self.bullet_joint_ids):
            p.resetJointState(self.robotId, bullet_joint_id,
                q[self.pinocchio_joint_ids[i] - 1], v[self.pinocchio_joint_ids[i] - 1],)

        # Update frames
        q, v = self.get_state()
        pin.forwardKinematics(self.model, self.data, q,v)
        pin.updateFramePlacements(self.model, self.data)

    def interpolate_control(self, control):
        """Match OCP and simulator timesteps by linear interpolation of the whole control sequence.

        ----Parameters----
        control     : np.array - Array that is being interpolated
        
        ----Returns-------
        new_len     : int - length of the inteprolated control
        new_control : np.array - Intepolated control sequence to be applied in pyBullet"""
        new_len = (self.interpolation_steps)*(control.shape[0]-1)+1
        new_control = zoom(control, (new_len/30,1))
        return new_len, new_control

    def match_models(self):
        """Update Pinnochio model to match the pyBullet model."""
        q,v = self.get_state()
        pin.forwardKinematics(self.model, self.data, q, v)
        pin.updateFramePlacements(self.model, self.data)

    def remove(self):
        """Remove the target from simulation window"""
        p.removeBody(self.robotId)