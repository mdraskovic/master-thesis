### Class for simulating a target in PyBullet
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.

import pybullet as p

class TargetSimulator:
    """
    ----Attributes----
    position : np.array([x,y,z])
    urdf_path : path to URDF file

    ----Methods------
    change()
        Remove old target and draw a new one
    add()
        Add another target with a new position
    remove()
        Remove the target from simulation window"""

    def __init__(self, position, urdf_path):
        self.position = position
        self.urdf_path = urdf_path
        self.new_position = None # For simulating 2 targets at a time; used for tracking controller

        # Load URDF
        self.baseOrientation = p.getQuaternionFromEuler([0, 0, 0])
        self.id = p.loadURDF(urdf_path, position, self.baseOrientation, useFixedBase=True)

    def change(self, new_position):
        """Remove old target and draw a new one."""
        self.position=new_position
        p.resetBasePositionAndOrientation(self.id, new_position, self.baseOrientation)

    def add(self, new_position):
        """Add another target with a new position. Used when there are 2 targets for the tracking controller."""
        self.id_new = p.loadURDF(self.urdf_path, new_position, self.baseOrientation, useFixedBase=True)
        self.new_position = new_position # Save new position

    def remove(self):
        """Remove the target from simulation window completelly."""
        p.removeBody(self.id)
        if self.new_position is not None:
            self.id = self.id_new
            self.position = self.new_position