### Script for solving 1 OCP
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import numpy as np
import matplotlib.pyplot as plt
import pybullet as p
from time import time, sleep
import pinocchio as pin
from ddp.reachingTask import ReachingTask
from simulators.armSimulator import ArmSimulator
from simulators.targetSimulator import TargetSimulator
import plots
import utils

#################################################
#------------------ Setup data ------------------
#################################################
target = np.array([0.25, 0.4, 0.3])

maxiter = 100
warmstart=False
callbacks=False
do_print=True

# Create robot arm
DT = utils.DT
T = utils.T
robot_urdf = utils.robot_urdf
mesh_dir = utils.mesh_dir
end_effector = utils.end_effector
target_urdf = utils.target_urdf
joint_names = utils.joint_names
pybDT = utils.pybDT

# Start simulation
p.connect(p.GUI)
p.setGravity(0, 0, -9.81)
p.setPhysicsEngineParameter(fixedTimeStep=pybDT, numSubSteps=1)
simulator = ArmSimulator(DT, robot_urdf, mesh_dir, joint_names, pybDT)
target_simulator = TargetSimulator(target, target_urdf) 
reaching_task = ReachingTask(simulator.model, simulator.data, end_effector, DT, T)

#################################################
#------------------ Solve OCP -------------------
#################################################
# Initialize DDP solver
q0,v0 = simulator.get_state()
x0 = np.concatenate([q0, v0])
reaching_task.init_solver(x0, target)

start_solve_time = time()
state_trajectory, control, _ = reaching_task.solve(x0, target, None, maxiter, warmstart,callbacks,do_print)
total_solve_time = time() - start_solve_time
print("OCP solve time = ", total_solve_time)

# Interpolate trajectories to match pyBullet frequency
new_len, new_control = simulator.interpolate_control(control)
_, new_state_trajectory = simulator.interpolate_control(state_trajectory)
G = np.array(reaching_task.ddp.K)  # Feedback gains

# Initialize variables to store values
duration = new_len #*2 # for 2 targets
t = np.arange(0, duration*pybDT, pybDT)
t = t[:-1]
torques = np.zeros((simulator.number_joints, duration))
joint_positions = np.zeros((simulator.number_joints, duration))
joint_velocities = np.zeros((simulator.number_joints, duration))
error_q_plot = np.zeros((simulator.number_joints, duration))
error_eef = np.zeros(duration)
ff_torques = np.zeros((simulator.number_joints, duration))
fb_torques = np.zeros((simulator.number_joints, duration))

### Simulate reaching task ###
j = -1 # iterates tru gains
k = 0  # iterates tru one interpolated part 

for i in range(new_len):
    k += 1
    if k==simulator.interpolation_steps: # Handling issue of gain matrix having smaller dimensions
        k = 0                        # by repeating gains for the interpolated parts
        j += 1

    q, v = simulator.get_state()
    pin.forwardKinematics(reaching_task.model, reaching_task.data, q) # Update pinnochio
    pin.updateFramePlacements(reaching_task.model, reaching_task.data)

    error_q = (q - new_state_trajectory[i][0:5]) 
    error_v = (v - new_state_trajectory[i][5:])
    fb = np.dot(G[j], np.concatenate((error_q, error_v)))
    tau = new_control[i] - fb

    # Save values for plotting
    torques[:,i] = tau
    joint_positions[:,i] = q
    joint_velocities[:,i] = v
    error_q_plot[:,i] = error_q
    eef_pos_in_world = np.array(p.getLinkState(simulator.robotId, 4)[0])
    error_eef[i] = np.linalg.norm(target - eef_pos_in_world)
    ff_torques[:,i] = new_control[i]
    fb_torques[:,i] = - fb

    simulator.send_joint_command(tau)
    simulator.step()
    sleep(1./1000.)

#################################################
#-------------------- Plots ---------------------
#################################################
plots.OCP(t, simulator.joint_names, torques, joint_positions, joint_velocities, error_q_plot, ff_torques, fb_torques, error_eef)
plt.show()
while True: # keep window open
    pass
p.disconnect()