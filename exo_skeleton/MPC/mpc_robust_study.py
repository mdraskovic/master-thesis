### Class for MPC controller tailored for robustness study
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import numpy as np
import pinocchio as pin
import pybullet as p 
from time import time 
import ddp.ddp_utils as ddp_utils
import plots
import utils

class MPC_ArmExo:
    """MPC that has 2 separate reaching tasks, for arm and exoskeleton.

    ----Attributes----
    simulator : PyBullet simulator that contains Pinnochio instabce (model and data)
    arm ,exo  : Controller instances

    ----Methods------
    run_static_target()
        MPC controller for 1 reaching task
    warmstart()
        Setup trajectory and control sequences used as initial guess
    apply_control()
        Apply FF torque
    update_plan()
        Solve OCP"""
    def __init__(self, simulator, arm, exo):
        self.simulator = simulator
        self.arm = arm
        self.exo = exo 

        self.pybDT = pybDT = utils.pybDT

        self.duration = round(max([arm.T_sim, exo.T_sim])/pybDT)
        self.duration_arm = round(arm.T_sim/pybDT)         # Normalized duration of simulation
        self.duration_exo = round(exo.T_sim/pybDT)
        self.number_replans_arm = round(self.duration_arm/(arm.mpcDT/pybDT)) # Number of times traj is replaned
        self.number_replans_exo = round(self.duration_exo/(exo.mpcDT/pybDT))
        self.timestamp_change_arm = []
        self.timestamp_change_exo = []

    def run_static_target(self, target_arm, target_exo, callbacks=False, do_print_DDP=False, do_print_MPC=True, do_plot=False):
        """Run MPC that replans with DDP for a static target.
        
        ----Parameters----
        target_arm, _exo : [x,y,z]
        callbacks        : bool - Display log of the DDP
        do_print_DDP     : bool - Print DDP output
        do_print_MPC     : bool - Print MPC output
        do_plot          : bool - Plot DDP trajectories"""
        pybDT = self.pybDT # For speed save locally
        arm = self.arm
        exo = self.exo
        arm_mpcDT = arm.mpcDT
        exo_mpcDT = exo.mpcDT
        duration_arm = self.duration_arm
        duration_exo = self.duration_exo

        # Init for plotting
        number_joints = len(self.simulator.joint_names)

        # Initialize variables to store values
        duration = self.duration
        torques_arm = np.zeros((number_joints, duration_arm))
        torques_exo = np.zeros((number_joints, duration_exo))
        torques   = np.zeros((number_joints, duration)) # Together
        joint_pos = np.zeros((number_joints, duration))
        joint_vel = np.zeros((number_joints, duration))
        error_eef = np.zeros(duration)

        plan_cnt_arm = 0        # Counter for plotting that goes through all plans
        MPC_plans_arm = np.zeros((self.number_replans_arm, arm.T, number_joints-1))  # 5th joint in exo (wrist) is useless
        plan_cnt_exo = 0       
        MPC_plans_exo = np.zeros((self.number_replans_exo, exo.T, number_joints-1))  
        
        plan_time_arm = 0       # Avarage time needed to plan/control
        plan_time_exo = 0
        ctrl_time = 0  
        self.arm.cnt_fail = 0   # Counter how many times DDP failed in the MPC task
        self.exo.cnt_fail = 0 

        control_exo = []; control_arm = [] # Initialize control sequence
        j_arm = -1              # Iterator through 1 plan, restarted every time traj is replaned
        j_exo = -1
        for i in range(duration):
            q,v = self.simulator.get_state()
            x0 = np.concatenate([q, v])


            ### Replan trajectory - Arm ###
            if i<duration_arm and (j_arm == -1 or j_arm == (arm_mpcDT/pybDT)):       # check if mpcDT has passed 

                ### Warmstarting ###
                xs0_arm, us0_arm = self.warmstart(arm, j_arm, control_arm, x0)
                
                start_solve_time = time()
                control_arm, MPC_plans_arm[plan_cnt_arm,:,:] = self.update_plan(arm, x0, target_arm, i, callbacks, do_print_DDP, do_plot, xs0_arm, us0_arm, exo=False)
                plan_time_arm += time() - start_solve_time
                plan_cnt_arm += 1

                j_arm = 0   # reset iterator through 1 plan
            
            ### Replan trajectory - Exo ###
            if i<duration_exo and (j_exo == -1 or j_exo == (exo_mpcDT/pybDT)):       # check if mpcDT has passed 

                ### Warmstarting ###
                xs0_exo, us0_exo = self.warmstart(exo, j_exo, control_exo, x0)

                start_solve_time = time()
                control_exo, MPC_plans_exo[plan_cnt_exo,:,:] = self.update_plan(exo, x0, target_exo, i, callbacks, do_print_DDP, do_plot, xs0_exo, us0_exo, exo=True)
                plan_time_exo += time() - start_solve_time
                plan_cnt_exo += 1

                j_exo = 0   # reset iterator through 1 plan


            ### Apply control torque ###
            start_ctrl_time = time()
            if i < duration_arm and i < duration_exo:
                tau =  control_arm[j_arm]*arm.torque/100 + control_exo[j_exo]*exo.torque/100
            elif i < duration_exo:
                tau =  control_exo[j_exo]*exo.torque/100
            else:
                tau =  control_arm[j_arm]*arm.torque/100
            
            self.apply_control(tau)
            ctrl_time += time() - start_ctrl_time

            # Save values for plotting
            if i < duration_arm:
                torques_arm[:,i] = control_arm[j_arm]
            if i < duration_exo:
                torques_exo[:,i] = control_exo[j_exo]
            torques[:,i] = tau
            joint_pos[:,i] = q
            joint_vel[:,i] = v
            eef_pos_in_world = np.array(p.getLinkState(self.simulator.robotId, number_joints-1)[0])
            error_eef[i] = np.linalg.norm(target_arm - eef_pos_in_world)

            j_arm += 1
            j_exo += 1

        if do_print_MPC: 
            print("ARM :")
            ddp_utils.MPC_statistics(plan_time_arm, ctrl_time, self.arm.cnt_fail, self.number_replans_arm, self.duration_arm)
            print("EXO :")
            ddp_utils.MPC_statistics(plan_time_exo, ctrl_time, self.exo.cnt_fail, self.number_replans_exo, self.duration_exo)
        if do_plot:
            plots.MPC_study(self, torques, torques_arm, torques_exo, joint_pos, joint_vel, error_eef, MPC_plans_arm, MPC_plans_exo)

    def warmstart(self, object, j, control, x0):
        """Setup trajectory and control sequences used as initial guess
        
        ----Parameters----
        object  : arm or exo
        j       : current iteration
        control : ctrl sequence
        x0      : Initial state"""
        xs0 = list(object.reaching_task.ddp.xs[:]) # + [self.reaching_task.ddp.xs[-1]]
        xs0[0] = x0
        us0 = list(object.reaching_task.ddp.us[:]) # + [self.reaching_task.ddp.us[-1]]
        if j!=-1:  
            us0[0] = control[int(object.mpcDT/self.pybDT)+1]
        return xs0, us0

    def apply_control(self, torque):
        """Apply FF torque (np.array containing values for all joints) to the simulator"""
        self.simulator.send_joint_command(torque)
        self.simulator.step()

    def update_plan(self, object, x0, target, i, callbacks, do_print_DDP, do_plot, xs0=None, us0=None, exo=True):
        """Solve OCP with a DDP algorithm. Replan a trajectory. 

        ----Parameters----
        object       : arm or exo
        x0           : np.array([ joint angles  ;  joint velocities ]) - initial state
        target       : np.array([x,y,z])
        i            : int  - current iteration
        callbacks    : bool - Display log of the DDP
        do_print_DDP : bool - Print DDP output
        do_plot      : bool - Plot DDP trajectories
        xs0, us0     : np.array - trajectory and control values for each joint used for warmstarting
        
        ----Returns------
        new_control : np.array - interpolated control sequence
        plan        : np.array - planned DDP trajectory (used for plotting)"""
        pin.forwardKinematics(object.model, object.data, x0[0:object.nq])
        pin.updateFramePlacements(object.model, object.data)

        # Resolve OCP
        reaching_task = object.reaching_task
        reaching_task.reset_cost_models()
        reaching_task.init_solver(x0, target, exo)
        trajectory, control, tf = reaching_task.solve(x0, target, i, object.maxiter, i, callbacks, do_print_DDP, xs0, us0) # Solve DDP

        if not tf:
            object.cnt_fail += 1

        if do_plot: # Save joint positions plan for shoulder (3D) and elbow. Used for plotting MPC plans
            number_joints = len(self.simulator.joint_names)
            plan = np.zeros((object.T, number_joints-1))
            traj_len = trajectory.shape[0]-1
            plan[0:traj_len, :] = trajectory[0:traj_len, 0:number_joints-1]
            if traj_len != object.T:                     # If the horizon is shortened
                    for i in range(traj_len, object.T):  # Adjust the size for plotting
                        plan[i,:] = None
        else:
            plan = None

        # Interpolate trajectory to match pyBullet frequency
        _, new_control = self.simulator.interpolate_control(control) 
        return new_control, plan