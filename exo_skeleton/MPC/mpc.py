### Class for MPC controller
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import numpy as np
import pinocchio as pin
import pybullet as p 
from time import time
import ddp.ddp_utils as ddp_utils
import plots
import ddp.weights as weights
import utils

class MPC:
    """MPC resolves OCP with a DDP algorithm from Crocoddyl library. 
    Feed-forward torque is sent and replans based on measurement.

    ----Attributes----
    simulator     : PyBullet simulator that contains Pinnochio instance (model and data)
    reaching_task : OCP
    mpcDT         : timestep for replanning
    T_simulation  : total length of simulation
    target_simulator : PyBullet simulator

    ----Methods------
    run_static_target()
        MPC controller for 1 reaching task
    run_tracking()
        MPC controller for tracking
    apply_control()
        Apply FF torque
    update_plan()
        Solve OCP
    """
    def __init__(self, simulator, reaching_task, mpcDT, T_simulation, target_simulator=None):
        self.simulator = simulator           
        self.reaching_task = reaching_task  
        self.mpcDT = mpcDT
        self.T_simulation = T_simulation
        self.target_simulator = target_simulator

        self.pinT = utils.T
        self.pinDT = utils.DT
        self.pybDT = pybDT = utils.pybDT

        self.model = simulator.model
        self.data = simulator.data
        self.nq = simulator.nq
        self.nv = simulator.nv

        self.duration = round(T_simulation/pybDT)                     # Normalized duration of simulation
        self.number_replans = round(self.duration/(mpcDT/pybDT)) # Number of times trajectory is replaned
        self.timestamp_change_weights = []

    def run_static_target(self, target, maxiter=100, callbacks=False, do_print_DDP=False, do_print_MPC=True, do_plot=True, shorten_horizon=False, time_varying=False):
        """Run MPC that replans with DDP for a static target.
        Possibly shortens horizon to prevent MPC thinking it has time - Not used in final version
        
        ----Parameters----
        target          : [x,y,z]
        maxiter         : int  - maximal iterations in DDP
        callbacks       : bool - Display log of the DDP
        do_print_DDP    : bool - Print DDP output
        do_print_MPC    : bool - Print MPC output
        do_plot         : bool - Plot DDP trajectories
        shorten_horizon : bool - indicates the horizon should be shortened to prevent MPC think it has time
        time_varying    : bool - indicates that frame translation cost has increasing cost with a tanh function"""
        reduce_T = False    # True-> number of running models reduced; False-> Terminal models repeated at the end

        if shorten_horizon:
            T = self.pinT   # Horizon for DDP (number of running models in DDP)
            start_shorten_timestep = self.T_simulation - self.pinDT*T # Time when the horizon should start shortening
            cnt_shorten = 0 # Counter used for shortening the horizon (0001->0011)
            cnt_pinDT_mpcDT_ratio = self.pinDT/self.mpcDT

        number_joints = self.simulator.number_joints
        duration = self.duration
        nq = self.nq
        if do_plot:         # Initialize variables to store values
            torques   = np.zeros((number_joints, duration))
            joint_pos = np.zeros((number_joints, duration))
            joint_vel = np.zeros((number_joints, duration))
            error_eef = np.zeros(duration)
        plan_cnt = 0        # Counter for plotting that goes through all plans
        MPC_plans = np.zeros((self.number_replans, self.pinT, number_joints-1))  # 5th joint in exo (wrist) is useless

        plan_time = 0       # Avarage time needed to plan/control
        ctrl_time = 0  
        self.cnt_fail = 0   # Counter how many times DDP failed in the MPC task

        if time_varying:    # Load initial frame weights
            _,running_weights, terminal_weights,_,_,_ = weights.load(nq, nq)

        j = -1  # Iterator through 1 plan (restarted every time traj is replanned)
        mpcDT = self.mpcDT
        pybDT = self.pybDT
        reaching_task = self.reaching_task
        for i in range(duration):
            q,v = self.simulator.get_state()
            x0 = np.concatenate([q, v])

            ### Replan trajectory ###
            if j == -1 or j == (mpcDT/pybDT):       # check if mpcDT has passed 

                ### Warmstarting ###
                xs0 = list(reaching_task.ddp.xs[:]) # + [reaching_task.ddp.xs[-1]]
                xs0[0] = x0
                us0 = list(reaching_task.ddp.us[:]) # + [reaching_task.ddp.us[-1]]
                if j!=-1:  
                    us0[0] = control[int(mpcDT/pybDT)+1]

                ### Shorten horizon ###
                if shorten_horizon:                           # Correcting warmstart bcs horizon was shortened for last target
                    if reduce_T and j == -1 and len(us0) != self.pinT:
                        xs0 = self.xs0_fullT
                        us0 = self.us0_fullT
   
                    if i*pybDT > start_shorten_timestep: # Check if the horizon should start shortening (at mpcT-pinT*pinDT)
                        cnt_shorten += 1                      # Since mpcDT<<pinDT, wait until pinDT passes
                        if cnt_shorten == cnt_pinDT_mpcDT_ratio:
                            if T==self.pinT:                  # First time the horizon is shortened
                                self.xs0_fullT = xs0          # Save values for warmstaring
                                self.us0_fullT = us0
                            if not self.timestamp_change_weights: # Add timestep when the horizon is reduced the first time
                                self.timestamp_change_weights.append(i)
                            if T>2:
                                T -= 1 
                            if reduce_T:
                                xs0 = xs0[0:T+1]
                                us0 = us0[0:T]
                            reaching_task.shorten_horizon(x0, T, reduce_T)
                            cnt_shorten = 0 

                ### Update time varying weights ###
                if time_varying:
                    reaching_task.update_time_varying_weights(i, self.duration, running_weights[0], terminal_weights[0])

                start_solve_time = time()
                control, MPC_plans[plan_cnt,:,:] = self.update_plan(x0, target, i, maxiter, callbacks, do_print_DDP, do_plot, xs0, us0)
                plan_time += time() - start_solve_time                

                plan_cnt += 1
                j = 0   # reset iterator through 1 plan

            ### Apply control torque ###
            start_ctrl_time = time()
            self.apply_control(control[j])
            ctrl_time += time() - start_ctrl_time

            if do_plot: # Save values for plotting
                torques[:,i] = control[j]
                joint_pos[:,i] = q
                joint_vel[:,i] = v
                eef_pos_in_world = np.array(p.getLinkState(self.simulator.robotId, number_joints-1)[0])
                error_eef[i] = np.linalg.norm(target - eef_pos_in_world)
            j += 1

        if do_print_MPC: 
            ddp_utils.MPC_statistics(plan_time, ctrl_time, self.cnt_fail, self.number_replans, self.duration)
        if do_plot:
            plots.MPC(self, torques, joint_pos, joint_vel, error_eef, MPC_plans)

    def run_tracking(self, targets, maxiter=100, callbacks=False, do_print_DDP=False, do_print_MPC=True, do_plot=False, time_varying=False):
        """MPC controller for tracking
        
        ----Parameters----
        targets      : np.array of targets [x,y,z]
        maxiter      : int  - maximal iterations in DDP
        callbacks    : bool - Display log of the DDP
        do_print_DDP : bool - Print DDP output
        do_print_MPC : bool - Print MPC output
        do_plot      : bool - Plot DDP trajectories
        time_varying : bool - indicates that frame translation cost has increasing cost with a tanh function"""
        number_joints = self.simulator.number_joints
        duration = self.duration
        pinT = self.pinT
        nq = self.nq
        simulator = self.simulator

        if do_plot:       # Initialize variables to store values
            torques = np.zeros((number_joints, duration))
            joint_pos = np.zeros((number_joints, duration))
            joint_vel = np.zeros((number_joints, duration))
            error_eef = np.zeros(duration)
        plan_cnt = 0      # Counter for plotting that goes through all plans
        MPC_plans = np.zeros((self.number_replans, pinT, number_joints-1))  # 5th joint in exo (wrist) is useless

        plan_time = 0     # Avarage time needed to plan/control
        ctrl_time = 0  
        self.cnt_fail = 0 # Counter how many times DDP failed in the MPC task

        if time_varying:    # Load initial frame weights
            _,running_weights, terminal_weights,_,_,_ = weights.load(nq, nq)

        flag_new_target = False   # Prevents multiple appended timestamp_change_weights
        j = -1  # Iterator through 1 plan (restarted every time traj is replanned)
        k = 0   # Counter for taking T targets from the 'targets' list
        for i in range(duration):
            q,v = simulator.get_state()
            x0 = np.concatenate([q, v])

            ### Replan trajectory ###
            if j == -1 or j == (self.mpcDT/self.pybDT):    # check if mpcDT has passed 

                start_solve_time = time()
                target_change = self.reaching_task.update_tracking_costs(targets[k:k+pinT, :]) 

                if time_varying: # Update time varying weights
                    self.reaching_task.update_time_varying_weights(i, duration, running_weights[0], terminal_weights[0])

                control, MPC_plans[plan_cnt,:,:] = self.update_plan(x0, None, i, maxiter, callbacks, do_print_DDP, do_plot)
                plan_time += time() - start_solve_time

                if target_change and not(flag_new_target): # Save timestep when the new target appears and add a new target in simulation
                    self.timestamp_change_weights.append(i)
                    self.target_simulator.add(targets[k + pinT, :])
                    flag_new_target = True
                elif not(target_change) and flag_new_target:
                    self.target_simulator.remove()
                    flag_new_target = False
                plan_cnt += 1
                k += 1
                j = 0   # Reset iterator through 1 plan

            ### Apply control torque ###
            start_ctrl_time = time()
            self.apply_control(control[j])
            ctrl_time += time() - start_ctrl_time

            if do_plot: # Save values for plotting
                torques[:,i] = control[j]
                joint_pos[:,i] = q
                joint_vel[:,i] = v
                eef_pos_in_world = np.array(p.getLinkState(self.simulator.robotId, number_joints-1)[0])
                error_eef[i] = np.linalg.norm(targets[k,:] - eef_pos_in_world)
            j += 1

        if do_print_MPC: 
            ddp_utils.MPC_statistics(plan_time, ctrl_time, self.cnt_fail, self.number_replans, self.duration)
        if do_plot:
            plots.MPC(self, torques, joint_pos, joint_vel, error_eef, MPC_plans)

    def apply_control(self, torque):
        """Apply FF torque (np.array containing values for all joints) to the simulator"""
        self.simulator.send_joint_command(torque)
        self.simulator.step()

    def update_plan(self, x0, target, i, maxiter, callbacks, do_print_DDP, do_plot, xs0=None, us0=None):
        """Solve OCP with a DDP algorithm. Replan a trajectory. 

        ----Parameters----
        x0           : np.array([ joint angles  ;  joint velocities ]) - initial state
        target       : np.array([x,y,z])
        i            : int  - current iteration
        maxiter      : int  - maximal iterations in DDP
        callbacks    : bool - Display log of the DDP
        do_print_DDP : bool - Print DDP output
        do_plot      : bool - Plot DDP trajectories
        xs0, us0     : np.array - trajectory and control values for each joint used for warmstarting
        
        ----Returns------
        new_control : np.array - interpolated control sequence
        plan        : np.array - planned DDP trajectory (used for plotting)"""
        pin.forwardKinematics(self.model, self.data, x0[0:self.nq])
        pin.updateFramePlacements(self.model, self.data)

        trajectory, control, tf = self.reaching_task.solve(x0, target, i, maxiter, i, callbacks, do_print_DDP, xs0, us0) # Solve DDP

        if not tf:
            self.cnt_fail += 1

        if do_plot: # Save joint positions plan for shoulder (3D) and elbow. Used for plotting MPC plans
            plan = np.zeros((self.pinT, self.simulator.number_joints-1))
            traj_len = trajectory.shape[0]-1
            plan[0:traj_len, :] = trajectory[0:traj_len, 0:self.simulator.number_joints-1]
            if traj_len != self.pinT:                     # If the horizon is shortened
                    for i in range(traj_len, self.pinT):  # Adjust the size for plotting
                        plan[i,:] = None
        else:
            plan = None

        # Interpolate trajectory to match simulator frequency
        _, new_control = self.simulator.interpolate_control(control) 
        return new_control, plan