### Script for open-loop controller robust study
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import numpy as np
import matplotlib.pyplot as plt
import pybullet as p
import time
from simulators.Controller import Controller
from simulators.armSimulator import ArmSimulator
from simulators.targetSimulator import TargetSimulator
import plots
import utils

#################################################
#------------------ Setup data ------------------
#################################################
arm_torque = 50 # Percantage of torque this controller gives 
exo_torque = 50

DT = utils.DT
T = utils.T
arm_urdf = utils.arm_urdf
exo_urdf = utils.exo_urdf
mesh_dir = utils.mesh_dir
end_effector = utils.end_effector
target_urdf = utils.target_urdf
joint_names = utils.joint_names
pybDT = utils.pybDT

# Target
# target = np.array([0.4, -0.2, -0.2])
target = np.array([3.8e-01, -4.7e-01, 6.8e-02]) # x_desired (Ground truth)
noise = np.random.normal(0, 0.0) # Add stdev here to simulate arm having different target than planned
target_error = target + noise        # x_desired + error

# Start simulation
p.connect(p.GUI)
p.setGravity(0, 0, -9.81)
p.setPhysicsEngineParameter(fixedTimeStep=pybDT, numSubSteps=1)
simulator = ArmSimulator(DT, arm_urdf, mesh_dir, joint_names, pybDT)
target_simulator = TargetSimulator(target, target_urdf)

#################################################
#------------------ Controller ------------------
#################################################
# Define arm and exo
arm = Controller(arm_urdf, mesh_dir, end_effector, DT, T, torque=arm_torque)
exo = Controller(exo_urdf, mesh_dir, end_effector, DT, T, torque=exo_torque)

# Initialize DDP solver
q0,v0 = simulator.get_state()
x0 = np.concatenate([q0, v0])
arm.reset(q0,v0)
exo.reset(q0,v0)
arm.reaching_task.init_solver(x0, target_error)
exo.reaching_task.init_solver(x0, target)
maxiter = 100
warmstart=False
callbacks=False
do_print=True

### Solve OCP ###
print("ARM:")
start_solve_time = time.time()
trajectory_arm, control_arm, _ = arm.reaching_task.solve(x0, target, None, maxiter, warmstart, callbacks, do_print)
total_solve_time = time.time() - start_solve_time
print("OCP solve time = ", total_solve_time)

print("EXO:")
start_solve_time = time.time()
trajectory_exo, control_exo, _ = exo.reaching_task.solve(x0, target_error, None, maxiter, warmstart, callbacks, do_print)
total_solve_time = time.time() - start_solve_time
print("Exo OCP solve time = ", total_solve_time)

# Interpolate trajectories to match pyBullet frequency
new_len, new_control_arm = simulator.interpolate_control(control_arm)
_, new_trajectory_arm = simulator.interpolate_control(trajectory_arm)
G_arm = np.array(arm.reaching_task.ddp.K)  # Feedback gains

_, new_control_exo = simulator.interpolate_control(control_exo)
_, new_trajectory_exo = simulator.interpolate_control(trajectory_exo)
G_exo = np.array(exo.reaching_task.ddp.K)  # Feedback gains

# Interpolate gain matrix
from scipy.ndimage import zoom
G_arm = zoom(G_arm, (1451/30,1,1))
G_exo = zoom(G_exo, (1451/30,1,1))


# Initialize variables to store values
duration = new_len
t = np.arange(0, duration*pybDT, pybDT)
# Arm
number_joints = simulator.number_joints
torques_arm = np.zeros((number_joints, duration))
error_q_plot_arm = np.zeros((number_joints, duration))
ff_torques_arm = np.zeros((number_joints, duration))
fb_torques_arm = np.zeros((number_joints, duration))

# Exo
torques_exo = np.zeros((number_joints, duration))
error_q_plot_exo = np.zeros((number_joints, duration))
ff_torques_exo = np.zeros((number_joints, duration))
fb_torques_exo = np.zeros((number_joints, duration))

# Together
torques = np.zeros((number_joints, duration))
joint_positions = np.zeros((number_joints, duration))
joint_velocities = np.zeros((number_joints, duration))
error_eef = np.zeros(duration)
ff_torques = np.zeros((number_joints, duration))
fb_torques = np.zeros((number_joints, duration))

### Simulate reaching task ###
for i in range(new_len):
    q, v = simulator.get_state()

    # FB gains
    error_q_arm = (q - new_trajectory_arm[i][0:5]) 
    error_v_arm = (v - new_trajectory_arm[i][5:])
    fb_arm = np.dot(G_arm[i], np.concatenate((error_q_arm, error_v_arm)))

    error_q_exo = (q - new_trajectory_exo[i][0:5]) 
    error_v_exo = (v - new_trajectory_exo[i][5:])
    fb_exo = np.dot(G_exo[i], np.concatenate((error_q_exo, error_v_exo)))

    tau_arm = new_control_arm[i] - fb_arm
    tau_exo = new_control_exo[i] - fb_exo

    tau = tau_arm*arm_torque/100 + tau_exo *exo_torque/100

    # Save values for plotting
    torques_arm[:,i] = tau_arm
    torques_exo[:,i] = tau_exo
    torques[:,i] = tau

    joint_positions[:,i] = q
    joint_velocities[:,i] = v

    error_q_plot_arm[:,i] = error_q_arm
    error_q_plot_exo[:,i] = error_q_exo

    eef_pos_in_world = np.array(p.getLinkState(simulator.robotId, 4)[0])
    error_eef[i] = np.linalg.norm(target - eef_pos_in_world)

    ff_torques_arm[:,i] = new_control_arm[i]
    fb_torques_arm[:,i] = - fb_arm
    ff_torques_exo[:,i] = new_control_exo[i]
    fb_torques_exo[:,i] = - fb_exo

    simulator.send_joint_command(tau)
    simulator.step()
    time.sleep(1./5000.)

#################################################
#-------------------- Plots ---------------------
#################################################
plots.OCP_study(t, simulator.joint_names, torques, torques_arm, torques_exo, joint_positions, joint_velocities, error_q_plot_arm, error_q_plot_exo, ff_torques_arm, ff_torques_exo, fb_torques_arm, fb_torques_exo, error_eef)
plt.show()
while True: # keep window open
    pass
p.disconnect()