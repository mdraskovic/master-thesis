### OCP for a reaching task solved with a (F)DDP algorithm from Crocoddyl library
### Tailored for MPC
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
sys.path.append(current)

import crocoddyl
import pinocchio as pin
import numpy as np
import ddp_utils
from cost_models import CostModels

class ReachingTask:
    """    
    ----Attributes----
    model, data : from Pinocchio instance
    end_effector : last link in the system -> "hand"
    DT, T        : OCP timestep and horizon
    
    ----Methods------
    reset_cost_models()
        Create completely new cost models
    init_solver()
        Create the FDDP solver
    solve()
        Solves the OCP. Designed for MPC
    inverse_kinematics()
        Get joint angles based on the end-effector position
    forward_kinematics()
        Get end-effector position based on the joint angles
    jacobian()
        Derivative of forward kinematics
    pinv()
        Right pseudo-inverse
    update_frames()
        For Pinocchio
    update_target()
        Change frame cost reference when the target changes
    shorten_horizon()
        Shorten horizon T in OCP
    update_tracking_costs()
        Update frame cost reference for tracking
    update_time_varying_weights()
        Have frame translation weights scaled along the OCP horizon
    cost_weight_tanh()
        Tanh function used to increase frame translation weights
    """
    
    def __init__(self, model, data, end_effector, DT, T):
        self.model = model             
        self.data = data                 
        self.end_effector = end_effector 
        self.DT = DT                     
        self.T = T                       

        self.nq = model.nq
        self.nv = model.nv

        self.cost_models = CostModels(model, data, end_effector, DT, T)

    def reset_cost_models(self):
        """Create completely new cost models"""
        self.cost_models = CostModels(self.model, self.data, self.end_effector, self.DT, self.T)

    def init_solver(self, x0, target, exo=True):
        """Create the FDDP solver. Create a shooting problem.
        
        ----Parameters----
        x0     : initial state [q,v]
        target : reaching goal [x,y,z]"""
        pin.forwardKinematics(self.model, self.data, x0[0:self.nq])
        pin.updateFramePlacements(self.model, self.data)

        self.unsolved_timestamp = [] # Used to save timestamps where DDP returend false
        self.target = target # Save target so that later we can check if it changed

        self.cost_models.add_basic_costs(x0, target, exo)
        running_models, terminal_model = self.cost_models.create_action_models()

        problem = crocoddyl.ShootingProblem(x0, running_models, terminal_model) 
        self.ddp = crocoddyl.SolverFDDP(problem)

    def solve(self, x0, target=None, i=None, maxiter=100, warmstart=True, callbacks=False, do_print=False, xs0=None, us0=None):
        """Solves the OCP and returns state and control trajectories. Designed to be used in MPC
        
        ----Parameters----
        x0     : initial state [q,v]
        target : reaching goal [x,y,z]. Default value is the one saved in class and it is used when the target does not change
        i      : Current iteratin within the MPC horizon
        maxiter    : maximal iterations in DDP
        warmstart  : warmstarting DDP state trajectory, used in MPC
        callbacks  : display log of the DDP
        do_print   : print DDP output
        do_plot    : plot DDP trajectories
        xs0, us0   : state and control values for each joint used for warmstart, used for shortening horizon - Not used in final version
        
        ----Returns------
        trajectory : state traj returned from OCP
        control    : control traj from OCP
        tf         : if the solver converges"""
        # Reinitialize the solver start state
        ddp = self.ddp
        ddp.problem.x0 = x0
        
        if callbacks :
            self.ddp.setCallbacks([crocoddyl.CallbackLogger(), crocoddyl.CallbackVerbose()])

        # Update problem when the target changes
        if target is not None and not(np.array_equal(target, self.target)):  
            self.update_target(target)

        # Warmstart and call solver
        if warmstart and (xs0 is None or us0 is None): 
            xs0 = list(ddp.xs[1:]) + [ddp.xs[-1]]
            xs0[0] = x0
            us0 = list(ddp.us[1:]) + [ddp.us[-1]] 
        elif not(warmstart):
            target = self.target
            target_state = self.inverse_kinematics(target)
            xs0 = [target_state for i in range(self.T+1)]
            us0 = ddp.problem.quasiStatic(xs0[:-1])  
        
        tf = ddp.solve(xs0, us0, maxiter, False)

        trajectory = np.array(ddp.xs)
        control = np.array(ddp.us)
        self.ddp = ddp
        self.update_frames() 

        if not(tf): # Save timestamp where DDP was unsuccessful
            self.unsolved_timestamp.append(i)
        if callbacks:
            ddp_utils.plot_DDP_callbacks(ddp)
        if do_print:
            ddp_utils.print_DDP_statistics(self, tf, i)

        return trajectory, control, tf

    def inverse_kinematics(self, p_des):
        """Get joint angles (q) based on the end-effector position (p_des)"""
        frame_id = self.model.getFrameId(self.end_effector)
        
        q  = np.array([0.0, -0.261799, 0.0, -0.523599, 0.0]) # Initial guess
        eps    = 0.01
        max_it = 1000
        alpha = 0.1
        
        for i in range(max_it):
            p = self.forward_kinematics( q, frame_id)
            err = (p - p_des)
            if np.linalg.norm(err) <= eps:
                break
            J = self.jacobian( q, frame_id)
            # q = q - alpha * self.pinv(J) @ err # Another way to do it
            v = - alpha * self.pinv(J) @ err  
            q = pin.integrate(self.model, q, v * self.DT)

        q = np.concatenate((q,v))  
        return q

    def forward_kinematics(self, q, frame_id):
        """Get the end-effector translation [x,y,z] of the link with id frame_id, based on the joint angles (q) """
        pin.forwardKinematics(self.model, self.data, q)
        pose = pin.updateFramePlacement(self.model, self.data, frame_id)
        return pose.translation

    def jacobian(self, q, frame_id):
        """Calculate the derivative of the forward kinematics function of a frame woth frame_id, at a given configuration q"""
        pin.framesForwardKinematics(self.model, self.data, q)
        pose = pin.updateFramePlacement(self.model, self.data, frame_id)
        body_jacobian = pin.computeFrameJacobian(self.model, self.data, q, frame_id)
        Ad = np.zeros((6, 6))
        Ad[:3, :3] = pose.rotation
        Ad[3:, 3:] = pose.rotation
        J = Ad @ body_jacobian
        Jlin = J[:3, :]
        return Jlin

    def pinv(self, J, eps=1e-3):
        """Calculate the right pseudoinverse of a matrix J"""
        JJT = J @ J.T
        return J.T @ np.linalg.inv((JJT + eps * np.eye(*JJT.shape)))

    def update_frames(self):
        """Update pinocchio frames."""
        xT = self.ddp.xs[-1]
        pin.forwardKinematics(self.model, self.data, xT[:self.nq], xT[self.nq:])
        pin.updateFramePlacements(self.model, self.data)

    def update_target(self, new_target):
        """Change frame cost reference when the target changes"""
        models = list(self.ddp.problem.runningModels) + [self.ddp.problem.terminalModel]

        if len(models) != self.T: # If the problem does not have a good horizon (if it was shortened, then a new problem must be created)
            self.target = new_target
            self.cost_models.remove_costs()
            self.init_solver(self.ddp.problem.x0, new_target) # Create a new solver shooting problem

        else: # Just the target needs to be changed; horizon is good
            self.unsolved_timestamp = [] # Used to save timestamps where DDP returned false
            self.target = new_target     # Save target so that later we can check if it changed
            for _,m in enumerate(models):
                m.differential.costs.costs['frameCost'].cost.residual.reference = new_target

    def shorten_horizon(self, x0, T_rm, reduce_T=False):
        """Shorten horizon for DDP. Used to prevent MPC of thinking it has time.
        
        ----Parameters----
        x0       : initial state [q,v]
        T_rm     : number of running models to be stacked up
        reduce_T : there are 2 ways to do so. True-> Number of knots T is reduced. False-> Terminal models are repeated"""
        running_models, terminal_model = self.cost_models.create_models_shorten_horizon(T_rm, reduce_T)
        problem = crocoddyl.ShootingProblem(x0, running_models, terminal_model) 
        self.ddp = crocoddyl.SolverDDP(problem)

    def update_tracking_costs(self, targets):
        """Update frame cost reference for tracking.
        
        ----Parameters----
        targets    : np.array([x,y,z], T)

        ----Returns------
        target_change : bool - if the target changed"""
        target_change = False

        ### Set frame cost in running models ###
        problem = self.ddp.problem
        running_models = list(problem.runningModels)
        i = 0 # Counter that goes tru all targets
        for _,m in enumerate(running_models):   # Set all weights for the current target
            m.differential.costs.costs['frameCost'].cost.residual.reference = targets[i,:] 
            i += 1

        ### Set frame cost in terminal model ###
        # Check if the target changes
        shape = targets.shape[0]
        for i in range((shape-1)):
            if not np.array_equal(targets[i], targets[i+1]):  # If there is a target change
                interpolated_target = targets[i] + (shape-i)*((targets[i+1]-targets[i])/shape) # Get an interpolation between 2 targets 
                target_change = True
                break

        terminal_model = [problem.terminalModel]
        for _,m in enumerate(terminal_model):   # Set all weights for the current target
            if target_change:
                m.differential.costs.costs['frameCost'].cost.residual.reference = interpolated_target 
            else: # W/o interpolation just put this part
                m.differential.costs.costs['frameCost'].cost.residual.reference = targets[-1,:] 

        return target_change

    def update_time_varying_weights(self, i, N, running_frame_weight, terminal_frame_weight):
        """Have weights scaled with a tanh function along the OCP horizon when the weights used are same for all OCPs.
        
        ----Parameters----
        i : int - current iteration
        N : int - total number of time steps"""
        cost_weight = self.cost_weight_tanh(i, N)
        problem = self.ddp.problem
        for _,m in enumerate(list(problem.runningModels)): # Running models
            m.differential.costs.costs['frameCost'].weight = running_frame_weight*cost_weight
        
        for _,m in enumerate([problem.terminalModel]):     # Terminal model
            m.differential.costs.costs['frameCost'].weight = terminal_frame_weight*cost_weight
        
    def cost_weight_tanh(self, i, N, lower=1, upper=10, alpha=10., alpha_cut=0.35):
        """Monotonically increasing weight profile over [0,...,N] based on a custom scaled hyperbolic tangent 
        
        ----Parameters----
        i         : int   - current iteration
        N         : int   - total number of time steps
        lower     : int   - lower value of tanh fun
        upper     : int   - value of the weight at the end of the window (must be >0)
        alpha     : float - controls the sharpness of the tanh (alpha high <=> very sharp)
        alpha_cut : float - shifts tanh over the time window (i.e. time of inflexion point)
        
        ----Returns------
        Cost weight at step i - starts at weight=0 (when i=0) and
        ends at weight<= upper (at i=N). As alpha --> inf, we tend toward upper"""
        return lower+0.5*(upper-lower)*( np.tanh(alpha*(i/N) -alpha*alpha_cut) + np.tanh(alpha*alpha_cut) )        