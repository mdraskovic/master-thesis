### Class for creating and managing cost models for an OCP solved by DDP algorithm from Crocoddyl library.
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import numpy as np
import crocoddyl
import weights
from utils import energyResidual, jointAccelerationResidual

class CostModels:
    """----Attributes----
    model, data : from Pinocchio instance
    end_effector : last link in the system -> "hand"
    DT, T        : OCP timestep and horizon

    ----Methods------
    create_action_models()
        Create differential and integral action models
    stack_up_models()
        Stack up IAMs in the running models
    add_basic_costs()
        Add constant costs for a reaching task to cost model sums
    remove_costs()
        Remove all costs from models
    create_models_shorten_horizon()
        Create custom models with shorter horizon - Not used in final version"""

    def __init__(self, model, data, end_effector, DT, T):
        self.model = model               
        self.data = data                 
        self.end_effector = end_effector 
        self.DT = DT                     
        self.T = T                      

        self.nq = model.nq               
        self.nv = model.nv 

        self.state = crocoddyl.StateMultibody(model)
        self.actuation = crocoddyl.ActuationModelFull(self.state)

        # Create cost model sums
        self.runningCostModel = crocoddyl.CostModelSum(self.state)
        self.terminalCostModel = crocoddyl.CostModelSum(self.state)

    def create_action_models(self):
        """Create differential and integral action models from cost model sums.

        ----Returns------
        running_models, terminal_model : Action models"""
        state = self.state
        actuation = self.actuation
        nq = self.nq

        # Create differential action models
        running_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, self.runningCostModel)
        terminal_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, self.terminalCostModel)
        
        # Create integrated action models
        running_model = crocoddyl.IntegratedActionModelEuler(running_DAM, self.DT)
        terminal_model = crocoddyl.IntegratedActionModelEuler(terminal_DAM)

        # Optionally add armature to take into account actuator's inertia
        running_model.differential.armature = np.array([.1]*nq)
        terminal_model.differential.armature = np.array([.1]*nq)

        running_models = self.stack_up_models(running_model, self.T)
        return running_models, terminal_model

    def stack_up_models(self, running_model, T_rm=None, second_model=None):
        """Stack up IAM s in the running models. 
        For the tracking controller, if there are 2 targets in the horizon, add the model for the 1st one in T_rm nodes and then add the 2nd.  
        
        ----Parameters-----
        running_model, second_model : Integrated action models with step DT
        T_rm : OCP horizon
        
        ----Returns------
        running_models : Stacked up running action models"""
        if second_model != None:
            running_models = [running_model] * T_rm + [second_model] * (self.T - T_rm)
        else:
            running_models = [running_model] * T_rm
        return running_models

    def add_basic_costs(self, x0, target, exo=True):
        """Add constant costs for a reaching task to cost model sums.
        
        Running cost model  : l(x,u) = framePlacement + regularizations + minimumEnergyCost + jointAcceleration + barriers
        Terminal cost model : l(x)   = framePlacement + stateReg + stateBarrier
        Refer to thesis reposrt for more details about the costs.
        
        ----Parameters----
        target : np.array([x,y,z])
        exo    : t/f - Indicates if the controller using this cost model is exoskeleton controller (and not human arm one). Used for robustness study"""
        # Load weights from IOC
        x_ref, running_w, terminal_w, uReg_w, xReg_w, frame_w= weights.load_learned(x0, target)
        
        if exo==False: # If the controller is used for an arm arm, change weights by value 'a'. Used for robustness study
            a = 0.0
            running_w[0] += a * running_w[0]
            terminal_w[0] += a * terminal_w[0]
            running_w[1] += a * running_w[1]
            running_w[6] += a *  running_w[6]

        ### Create all costs ###
        state = self.state
        actuation = self.actuation
        nu = actuation.nu
        nq = self.nq
        model = self.model

        # Frame translation costs
        frameResidual = crocoddyl.ResidualModelFrameTranslation(state, model.getFrameId(self.end_effector), target)
        frameActivation = crocoddyl.ActivationModelWeightedQuad(frame_w) 
        frameCost = crocoddyl.CostModelResidual(state, frameActivation, frameResidual)
        # State regularization cost ||x-x_ref||
        xRegResidual = crocoddyl.ResidualModelState(state, x_ref, nu)
        xActivation = crocoddyl.ActivationModelWeightedQuad(xReg_w)
        xRegCost = crocoddyl.CostModelResidual(state, xActivation, xRegResidual)
        # Control regularization cost (Warmstart with gravity compensation) ||u-u_ref||
        uRegResidual = crocoddyl.ResidualModelControlGrav(state, nu)
        uActivation = crocoddyl.ActivationModelWeightedQuad(uReg_w)
        uRegCost = crocoddyl.CostModelResidual(state, uActivation, uRegResidual)                                         
        # State limits penalizations
        xLimitResidual = crocoddyl.ResidualModelState(state, np.zeros(nq+nq), nu)
        xLimitActivation = crocoddyl.ActivationModelQuadraticBarrier(crocoddyl.ActivationBounds(state.lb, state.ub))
        xLimitCost = crocoddyl.CostModelResidual(state, xLimitActivation, xLimitResidual)
        # Control limits penalizations
        umax = np.array([150,70,100,75,0])  # Values obtained by observing human data
        umin = np.array([-150,-200,-170,-125,0])
        uLimitResidual = crocoddyl.ResidualModelControl(state, np.zeros(nq))
        uLimitActivation = crocoddyl.ActivationModelQuadraticBarrier(crocoddyl.ActivationBounds(umin, umax))
        uLimitCost = crocoddyl.CostModelResidual(state, uLimitActivation, uLimitResidual)
        # Energy cost
        energyRes = energyResidual(state, nu)
        energyActivation = crocoddyl.ActivationModelSmooth1Norm(nu)
        energyCost = crocoddyl.CostModelResidual(state, energyActivation, energyRes)
        # Angle acceleration
        accelerationReg = jointAccelerationResidual(state, nu, model, self.data)
        accelerationActivation = crocoddyl.ActivationModelWeightedQuad(uReg_w)
        accCost = crocoddyl.CostModelResidual(state, accelerationActivation, accelerationReg)

        self.runningCostModel.addCost("frameCost", frameCost, running_w[0]) 
        self.runningCostModel.addCost("xRegCost", xRegCost, running_w[1]) 
        self.runningCostModel.addCost("uRegCost", uRegCost, running_w[2]) 
        self.runningCostModel.addCost("xLimitCost", xLimitCost, running_w[3]) 
        self.runningCostModel.addCost("uLimitCost", uLimitCost, running_w[4]) 
        # self.runningCostModel.addCost("energyCost", energyCost, running_w[5]) # we caluclated with IOC that this cost is not used
        self.runningCostModel.addCost("accelerationCost", accCost, running_w[6]) 

        self.terminalCostModel.addCost("frameCost", frameCost, terminal_w[0]) 
        self.terminalCostModel.addCost("xRegCost", xRegCost, terminal_w[1]) 
        self.terminalCostModel.addCost("xLimitCost", xLimitCost, terminal_w[2]) 

        # Save values for frame cost. Used to shorten horizon
        self.frameCost = frameCost
        self.terminal_frame_weight = terminal_w[0]

    def remove_costs(self):
        """Remove costs when the target is updated""" 
        self.runningCostModel.removeCost("frameCost") 
        self.runningCostModel.removeCost("xRegCost")
        self.runningCostModel.removeCost("uRegCost")
        self.runningCostModel.removeCost("xLimitCost")
        self.runningCostModel.removeCost("uLimitCost")
        self.runningCostModel.removeCost("energyCost")
        self.runningCostModel.removeCost("accelerationCost")
        
        self.terminalCostModel.removeCost("frameCost")
        self.terminalCostModel.removeCost("xRegCost")
        self.terminalCostModel.removeCost("xLimitCost")

    def create_models_shorten_horizon(self, T_rm, reduce_T=False):
        """Create custom running models with shorter horizon. Used to prevent MPC of thinking it has time.
        Increase frame weights in terminal cost 10 times in the end of MPC.
        Not used in final version. Not used when the weights are learned and come from the IOC model.
        
        ----Parameters----
        T_rm     : int  - number of running models to be stacked up
        reduce_T : bool - There are 2 ways to do so. True-> Number of knots T is reduced. False-> Terminal models are repeated
        
        ----Returns------
        running_models, terminal_model : Action models"""
        state = self.state
        actuation = self.actuation
        nq = self.nq

        running_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, self.runningCostModel)
        running_model = crocoddyl.IntegratedActionModelEuler(running_DAM, self.DT)
        
        if reduce_T:
            running_models = self.stack_up_models(running_model, T_rm)
        else:
            t_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, self.terminalCostModel)
            t_model = crocoddyl.IntegratedActionModelEuler(t_DAM, self.DT)
            running_models = self.stack_up_models(running_model, T_rm, t_model)

        self.terminalCostModel.removeCost("frameCost")
        self.terminalCostModel.addCost("frameCost", self.frameCost, self.terminal_frame_weight*10) 
        terminal_DAM = crocoddyl.DifferentialActionModelFreeFwdDynamics(state, actuation, self.terminalCostModel)
        terminal_model = crocoddyl.IntegratedActionModelEuler(terminal_DAM)

        # Add armature to take into account actuator's inertia
        running_model.differential.armature = np.array([.1]*nq)
        terminal_model.differential.armature = np.array([.1]*nq)
        return running_models, terminal_model