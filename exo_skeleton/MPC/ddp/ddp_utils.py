### Utils for DDP
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
g_parent = os.path.dirname(parent)
sys.path.append(current)
sys.path.append(parent) 
sys.path.append(g_parent) 

import crocoddyl
import numpy as np
import csv
import torch
import utils

### Specify NN model file
model_file = "/home/mdraskovic/teza/exo_skeleton/ML/model_setup4"

### Functions ###
def find_mean_stdev(csv_file):
    """Calculation of the mean and standard deviation of the specified csv file."""
    # Open csv file
    with open(csv_file, 'r') as results_file: # Open in read mode
        csv_reader = csv.reader(results_file)  # Load csv data
        data = np.array(list(csv_reader)) 
    data = data[1:,:]                          # Remove header

    # Find mean and var of weights to scale them
    len_data = data.shape[0]
    Y = torch.zeros((len_data, 4))        # Weights
    # Load data to tensors
    for row in range(len_data):           # Go through all rows
        Y[row, :] = torch.from_numpy(utils.get_element(data, row, 8))

    mean_w1, std_w1 = torch.std_mean(Y[:,0])
    mean_w2, std_w2 = torch.std_mean(Y[:,1])
    mean_w3, std_w3 = torch.std_mean(Y[:,2])
    mean_w4, std_w4 = torch.std_mean(Y[:,3])
    print("------Mean and stdev of the csv file------")
    print('{0:.16f}'.format(mean_w1), '{0:.16f}'.format(std_w1) )
    print('{0:.16f}'.format(mean_w2), '{0:.16f}'.format(std_w2) )
    print('{0:.16f}'.format(mean_w3), '{0:.16f}'.format(std_w3) )
    print('{0:.16f}'.format(mean_w4), '{0:.16f}'.format(std_w4) )

    return mean_w1, std_w1, mean_w2, std_w2, mean_w3, std_w3, mean_w4, std_w4

def print_DDP_statistics(reaching_task, tf, i):
    """Print details about a solution of OCP"""
    #Get end effector from DDP to calculate error
    final_effector_position = np.array(reaching_task.data.oMf[reaching_task.model.getFrameId(reaching_task.end_effector)].translation.T.flat)

    print("_______________________")
    print("Target is ", reaching_task.target)
    print("Problem solved:", tf)
    print("Number of iterations:", reaching_task.ddp.iter)
    print("Total cost:", reaching_task.ddp.cost)
    print("Gradient norm:", reaching_task.ddp.stoppingCriteria())
    print("MPC iteration ", i)
    print('Finally reached = ', final_effector_position)
    print('Distance between hand and target = ', np.linalg.norm(final_effector_position - reaching_task.target))

def plot_DDP_callbacks(ddp):
    """Plot OPC solution and convergence. Callbacks must be initialized with 
        -> ddp.setCallbacks([crocoddyl.CallbackLogger(), crocoddyl.CallbackVerbose()])"""
    log = ddp.getCallbacks()[0]
    crocoddyl.plotOCSolution(log.xs, log.us, figIndex=1, show=False)
    crocoddyl.plotConvergence(log.costs, log.u_regs, log.x_regs, log.grads, log.stops, log.steps, figIndex=2)

def MPC_statistics(plan_time, ctrl_time, cnt_fail, number_replans, duration):
    """Print details about MPC."""
    if number_replans != 0:
        print("Avarage OCP solver time = ", plan_time/number_replans)
    print("Avarage control time = ", ctrl_time/duration)
    print("Number of fail times: ", cnt_fail)
