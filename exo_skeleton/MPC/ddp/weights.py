### Functions for hand-tuned and IOC-inferred weights
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 25. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
g_parent = os.path.dirname(parent)
sys.path.append(current)
sys.path.append(parent) 
sys.path.append(g_parent) 

import numpy as np
import torch
import ddp_utils
from utils import WeightsModel, create_w
from ddp_utils import find_mean_stdev

def load(nq, nv):
    """Load hand-tuned weights.
    
    ----Parameters----
    nq : int - number of joint states
    nv : int - number of joint velocities"""
    v_ref = np.array([0.0, 0.0, 0.0, 0.0, 0.0])        
    q_ref = np.array([0.0, -0.261799, 0.0, -0.523599, 0.0])  
    x_ref = np.concatenate([q_ref, v_ref])

    running_w = [0.085, 0.0001, 0.00001, 100, 0.01, 0.0, 0.01] # frameTranslation, xReg, uReg, xLim, uLim, energy, angleAcceleration
    terminal_w =[0.8, 0.0001, 100]                  # frameTranslation, xReg, xLim

    uReg_w = np.array([1.]*nq)
    xReg_w = (np.array([1.]*nq + [1.]*nv)) 
    xReg_w[2] = 5 # Higher weight for shoulder roll as it should be less moved
    xReg_w[7] = 5
    frame_w = np.array([1., 1., 1.])
    return x_ref, running_w, terminal_w, uReg_w, xReg_w, frame_w

def load_learned(x0, target, nn_model=None):
    """"Create a set of weights based on a neural network model learned from the real data using IOC.
    
    ----Parameters----
    x0 : Initial state [q,v]
    target : Reaching goal [x,y,z]
    nn_model : neural network model that outputs weights based on x0, target"""
    model_file = ddp_utils.model_file
    x_ref, running_w, terminal_w, uReg_w, xReg_w, frame_w = load(5,5)

    # Define NN ML model
    input_size = 11
    output_size = 4
    nn_model = WeightsModel(input_size, output_size)
    nn_model.load_state_dict(torch.load(model_file))
    nn_model.eval()

    ### Uncomment and run this section to find mean and stdev of the csv file ###
    # csv_file = 'setup4.csv'
    # mean_w1, std_w1, mean_w2, std_w2, mean_w3, std_w3, mean_w4, std_w4 = find_mean_stdev(csv_file)
        
    # Specify mean and stdev from csv file if known to speed up execution
    # setup4
    mean_w1 = 40.0421714782714844
    std_w1 = 52.2785491943359375
    mean_w2 = 1.6887873411178589
    std_w2 = 0.8365236520767212
    mean_w3 = 3.5812709331512451
    std_w3 = 2.7026033401489258
    mean_w4 = 10.5506258010864258
    std_w4 = 98.5788269042968750

    # Define input to model [X0, target]
    input = torch.zeros(11)
    input[:4] = torch.from_numpy(x0[:4])             
    input[4:8] = torch.from_numpy(x0[5:9])
    input[8:] = torch.from_numpy(target)

    w = nn_model(input).detach().numpy()[0]
    w[0] = w[0]*std_w1 + mean_w1 # De-normalize weights
    w[1] = w[1]*std_w2 + mean_w2
    w[2] = w[2]*std_w3 + mean_w3
    w[3] = w[3]*std_w4 + mean_w4
    w[w <0.0001] = 0.0001 # Set bounds
    w[w >100.0] = 100.0

    running_w, terminal_w = create_w(w)
    return x_ref, running_w, terminal_w, uReg_w, xReg_w, frame_w