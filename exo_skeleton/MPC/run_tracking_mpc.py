### Script for MPC tracking
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import numpy as np
import pybullet as p
from simulators.armSimulator import ArmSimulator
from ddp.reachingTask import ReachingTask
from simulators.targetSimulator import TargetSimulator
from mpc import MPC
import utils 

#################################################
#------------------ Setup data ------------------
#################################################
target0 = np.array([3.8e-01, -4.7e-01, 6.8e-02])
target1 = np.array([3.8e-01, -4.7e-01, 6.8e-02])
target2 = np.array([3.8e-01, -4.7e-01, 6.8e-02])
target3 = np.array([3.8e-01, -4.7e-01 -0.03, 6.8e-02])
target4 = np.array([3.8e-01, -4.7e-01 -0.05, 6.8e-02])
target5 = np.array([3.8e-01, -4.7e-01 -0.05, 6.8e-02-0.04])
target_sequence = [target0, target1, target2, target3, target4, target5]

# Create robot arm
DT = utils.DT
T = utils.T
robot_urdf = utils.robot_urdf
mesh_dir = utils.mesh_dir
end_effector = utils.end_effector
target_urdf = utils.target_urdf
joint_names = utils.joint_names
pybDT = utils.pybDT

# Start simulation
p.connect(p.GUI)
p.setGravity(0, 0, -9.81)
p.setPhysicsEngineParameter(fixedTimeStep=pybDT, numSubSteps=1)
simulator = ArmSimulator(DT, robot_urdf, mesh_dir, joint_names, pybDT)
target_simulator = TargetSimulator(target_sequence[0], target_urdf)  # Instance in the simulator
reaching_task = ReachingTask(simulator.model, simulator.data, end_effector, DT, T) 

# Initialize MPC
mpcDT = 0.005 # Replaning timestep (0.005 s)
T1_simulation = 0.5 # How many seconds is taken for 1 target
T_simulation = T1_simulation* len(target_sequence) # Total duration of the simulation in seconds
maxiter = 100
callbacks = False
do_print_DDP = False
do_print_MPC = True
do_plot = True
time_varying_weights = False

#################################################
#--------------------- MPC ----------------------
#################################################
mpc = MPC(simulator, reaching_task, mpcDT, T_simulation, target_simulator)

# Create a tracking sequence
number_T1 = T1_simulation/mpcDT # How many replans for 1 target
targets = np.zeros((mpc.number_replans + T, 3))  # Number of targets is how many DDPs need to be solved+the horizon
target_cnt = 1
for i in range(mpc.number_replans):
    targets[i,:] = target_sequence[target_cnt-1]
    if i==number_T1*target_cnt:
        target_cnt += 1
for i in range(mpc.number_replans, mpc.number_replans+T):
    targets[i,:] = target_sequence[-1]

# Initialize DDP solver
q0,v0 = simulator.get_state()
x0 = np.concatenate([q0, v0])
reaching_task.init_solver(x0, target_simulator.position)

mpc.run_tracking(targets, maxiter, callbacks, do_print_DDP, do_print_MPC, do_plot=True, time_varying = time_varying_weights)