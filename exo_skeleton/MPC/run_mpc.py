### Script for MPC
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(current)
sys.path.append(parent) 

import numpy as np
import pybullet as p
from time import  sleep
from ddp.reachingTask import ReachingTask
from simulators.armSimulator import ArmSimulator
from simulators.targetSimulator import TargetSimulator
from mpc import MPC
import utils

#################################################
#------------------ Setup data ------------------
#################################################
target1 = np.array([0.25, 0.4, 0.3])
target_sequence = [target1] # You can stack up targets here
do_plot = [False] # If it should be plotted

maxiter = 100
warmstart=False
callbacks=False
do_print_DDP = False
do_print_MPC = True

# Initialize MPC
mpcDT = 0.005    # Replaning timestep (0.005 s)
T_simulation = 2 # Total duration of the simulation in seconds
shorten_horizon = False
time_varying_weights = False

# Create robot arm
DT = utils.DT
T = utils.T
robot_urdf = utils.robot_urdf
mesh_dir = utils.mesh_dir
end_effector = utils.end_effector
target_urdf = utils.target_urdf
joint_names = utils.joint_names
pybDT = utils.pybDT

# Start simulation
p.connect(p.GUI)
p.setGravity(0, 0, -9.81)
p.setPhysicsEngineParameter(fixedTimeStep=pybDT, numSubSteps=1)
simulator = ArmSimulator(DT, robot_urdf, mesh_dir, joint_names, pybDT)
target = TargetSimulator(target_sequence[0], target_urdf) 
reaching_task = ReachingTask(simulator.model, simulator.data, end_effector, DT, T) 

#################################################
#--------------------- MPC ----------------------
#################################################
# Initialize DDP solver
q0,v0 = simulator.get_state()
x0 = np.concatenate([q0, v0])
reaching_task.init_solver(x0, target.position)

mpc = MPC(simulator, reaching_task, mpcDT, T_simulation)

for i in range(len(target_sequence)):
    target.change(target_sequence[i]) 
    mpc.run_static_target(target_sequence[i], maxiter, callbacks, do_print_DDP, do_print_MPC, do_plot=do_plot[i], shorten_horizon=shorten_horizon, time_varying = time_varying_weights)

while True: # keep window open
    pass
p.disconnect()