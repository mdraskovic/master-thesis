### Plot functions for OCP and MPC
### Author : Marina Draskovic 
### Email  : mdraskovic@ethz.ch 
### Date   : 26. August 2022.
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
plt.rcParams['axes.grid'] = True

plt.rc('legend', fontsize=14)
plt.rc('axes', labelsize=14)

def OCP(t, joint_names, tau, joint_pos, joint_vel, error_q, ff_tau, fb_tau, error_eef):
    """Plot results of 1 OCP for a 4D robot.
    
    ----Parameters----
    joint_names : list of strings of joint names from URDF file
    tau, joint_pos, joint_vel, error_q, ff_tau, fb_tau, error_eef : 2D matrices of values for plotting for each of 4 joints"""
    rad_to_deg = 180.0/3.141592653589793  
    fig1, axs1 = plt.subplots(2, 2) # Joint torque
    fig2, axs2 = plt.subplots(2, 2) # Joint positions
    fig3, axs3 = plt.subplots(2, 2) # Joint velocities
    fig4, axs4 = plt.subplots(2, 2) # Joint position error (x - x_ref)
    fig5, axs5 = plt.subplots()     # End effector error
    fig6, axs6 = plt.subplots(2, 2) # FF and FB torque components

    ctr = -1
    for i in [0,1]:
        for j in [0,1]:
            ctr += 1
            axs1[i,j].plot(t, tau[ctr,:], linewidth=2.0)
            axs1[i,j].set(xlabel=r"Time [s]", ylabel='Torque', title=joint_names[ctr])

            axs2[i,j].plot(t, joint_pos[ctr,:]*rad_to_deg, linewidth=2.0)
            axs2[i,j].set(xlabel=r"Time [s]", ylabel='Position [deg]', title=joint_names[ctr])

            axs3[i,j].plot(t, joint_vel[ctr,:]*rad_to_deg, linewidth=2.0)
            axs3[i,j].set(xlabel=r"Time [s]", ylabel='Velocity [deg/s]', title=joint_names[ctr])

            axs4[i,j].plot(t, error_q[ctr,:]*rad_to_deg, linewidth=2.0)
            axs4[i,j].set(xlabel=r"Time [s]", ylabel='Offset [deg]', title=joint_names[ctr])

            axs6[i,j].plot(t, fb_tau[ctr,:], linewidth=2.0, color="r")
            axs6[i,j].plot(t, ff_tau[ctr,:], linewidth=2.0)
            axs6[i,j].set(xlabel=r"Time [s]", ylabel='Torque', title=joint_names[ctr])
            axs6[i,j].legend(["FB", "FF"], loc=0)

    axs5.plot(t, error_eef*100, linewidth=2.0)
    axs5.set(xlabel=r"Time [s]", ylabel=r"Distance [deg]")
    
    fig1.suptitle("Joint torque")
    fig2.suptitle("Joint position")
    fig3.suptitle("Joint velocity")
    fig4.suptitle("Offset between Pinnochio and pyBullet positions in each step")
    fig5.suptitle("End effector distance to the target position")
    fig6.suptitle("FF & FB torque")

def MPC(MPC, torques, joint_pos, joint_vel, error_eef, MPC_plans):
    """Plot states, control, MPC plans

    ----Parameters----
    MPC : instance of mpc class that contains reaching_task , T_simulation, pinT, mpcDT, pinDT
    torques, joint_pos, joint_vel, error_eef : 2D matrices of values for plotting for each of 4 joints
    MPC_plans : 3D Matrix that contains all MPC planned trajectories for 4 joints (shoulder 3D and elbow)"""
    t = np.arange(0, MPC.T_simulation, MPC.pybDT)
    index_change_weights = [MPC.pybDT*(x) for x in MPC.timestamp_change_weights]

    # Define figures
    fig1, axs1 = plt.subplots(1, 4, figsize=plt.figaspect(0.3)) # Joint torques
    fig2, axs2 = plt.subplots(2, 2) # Joint positions
    fig3, axs3 = plt.subplots(2, 2) # Joint velocities
    fig5, axs5 = plt.subplots()     # End effector error
    fig6, axs6 = plt.subplots(1, 4, figsize=plt.figaspect(0.3))  # MPC trajectories

    subplot_titles = ["Shoulder Yaw", "Shoulder Pitch", "Shoulder Roll", "Elbow Pitch"]
    rad_to_deg = 180.0/3.141592653589793  

    for j in [0,1,2,3]:
        #Plot torque
        axs1[j].plot(t, torques[j,:], linewidth=1.5, color="black")
        axs1[j].set(xlabel=r"Time [s]", title=subplot_titles[j])
        value_change_weights = [torques[j][k] for k in MPC.timestamp_change_weights] # Adding points where the weights changed
        axs1[j].plot(index_change_weights, value_change_weights, "ob")
    axs1[0].set(ylabel=r'$\tau$[Nm]')

    ctr = -1  # Counter for joints: 0,1,2=shoulder x,y,z ; 3=elbow
    for i in [0,1]:
        for j in [0,1]:
            ctr += 1

            # Plot joint position
            axs2[i,j].plot(t, joint_pos[ctr,:]*rad_to_deg, linewidth=2.0)
            axs2[i,j].set(xlabel=r"Time [s]", ylabel='joint position [deg]', title=subplot_titles[ctr])
            value_change_weights = [joint_pos[ctr][k]*rad_to_deg for k in MPC.timestamp_change_weights] 
            axs2[i,j].plot(index_change_weights, value_change_weights, "ob")

            # Plot joint velocity
            axs3[i,j].plot(t, joint_vel[ctr,:]*rad_to_deg, linewidth=2.0)
            axs3[i,j].set(xlabel=r"Time [s]", ylabel='joint velocity [deg/s]', title=subplot_titles[ctr])
            value_change_weights = [joint_vel[ctr][k]*rad_to_deg for k in MPC.timestamp_change_weights]
            axs3[i,j].plot(index_change_weights, value_change_weights, "ob")

    # Plot distance to target
    axs5.plot(t, error_eef*100, linewidth=1.5, color='black')
    axs5.set(xlabel=r"Time [s]", ylabel=r"Distance to target [cm]")
    value_change_weights = [error_eef[k]*100 for k in MPC.timestamp_change_weights] 
    axs5.plot(index_change_weights, value_change_weights, "ob")

    # Plot MPC trajecotries - fig 6=Elbow, fig7,8,9=shoulder x,y,z
    cc_arm=['xkcd:barney purple', 'xkcd:light eggplant', 'xkcd:ugly purple', 'xkcd:purple', 'xkcd:orchid']
    cc_exo=['xkcd:orange', 'xkcd:faded orange', 'xkcd:tangerine', 'xkcd:pastel orange', 'xkcd:pumpkin']

    horizon = MPC.pinDT*(MPC.pinT) # Moving horizon
    for i in range(MPC.number_replans):
        t_mpc = np.linspace(i*MPC.mpcDT, round((i*MPC.mpcDT + horizon), 2) , MPC.pinT)
        axs6[0].plot(t_mpc, MPC_plans[i,:,0]*rad_to_deg, linewidth=1.5,color=cc_arm[i%len(cc_arm)], alpha=0.3) # shoulder x
        axs6[1].plot(t_mpc, MPC_plans[i,:,1]*rad_to_deg, linewidth=1.5,color=cc_arm[i%len(cc_arm)], alpha=0.3) # shoulder y
        axs6[2].plot(t_mpc, MPC_plans[i,:,2]*rad_to_deg, linewidth=1.5,color=cc_arm[i%len(cc_arm)], alpha=0.3) # shoulder z
        axs6[3].plot(t_mpc, MPC_plans[i,:,3]*rad_to_deg, linewidth=1.5,color=cc_arm[i%len(cc_arm)], alpha=0.3) # elbow
        axs6[0].plot(t_mpc, MPC_plans[i,:,0]*rad_to_deg, linewidth=1.0,color=cc_arm[i%len(cc_exo)], alpha=0.2) # shoulder x
        axs6[1].plot(t_mpc, MPC_plans[i,:,1]*rad_to_deg, linewidth=1.0,color=cc_arm[i%len(cc_exo)], alpha=0.2) # shoulder y
        axs6[2].plot(t_mpc, MPC_plans[i,:,2]*rad_to_deg, linewidth=1.0,color=cc_arm[i%len(cc_exo)], alpha=0.2) # shoulder z
        axs6[3].plot(t_mpc, MPC_plans[i,:,3]*rad_to_deg, linewidth=1.0,color=cc_arm[i%len(cc_exo)], alpha=0.2) # elbow
    axs6[0].set(xlabel=r"Time [s]", ylabel='Position [deg]', title='Shoulder Yaw')
    axs6[1].set(xlabel=r"Time [s]", title='Shoulder Pitch')
    axs6[2].set(xlabel=r"Time [s]", title='Shoulder Roll')
    axs6[3].set(xlabel=r"Time [s]", title='Elbow Pitch')
     # Add real trajectory over the MPC plans
    axs6[0].plot(t, joint_pos[0,:]*rad_to_deg, linewidth=1.5, color="black")
    axs6[1].plot(t, joint_pos[1,:]*rad_to_deg, linewidth=1.5, color="black")
    axs6[2].plot(t, joint_pos[2,:]*rad_to_deg, linewidth=1.5, color="black")
    axs6[3].plot(t, joint_pos[3,:]*rad_to_deg, linewidth=1.5, color="black")

    value_change_weights = [joint_pos[0,k]*rad_to_deg for k in MPC.timestamp_change_weights] 
    axs6[0].plot(index_change_weights, value_change_weights, "ob")
    value_change_weights = [joint_pos[1,k]*rad_to_deg for k in MPC.timestamp_change_weights] 
    axs6[1].plot(index_change_weights, value_change_weights, "ob")
    value_change_weights = [joint_pos[2,k]*rad_to_deg for k in MPC.timestamp_change_weights] 
    axs6[2].plot(index_change_weights, value_change_weights, "ob")       
    value_change_weights = [joint_pos[3,k]*rad_to_deg for k in MPC.timestamp_change_weights] # Add timestemp where weights changed
    axs6[3].plot(index_change_weights, value_change_weights, "ob") 

    # Set figure titles
    # fig1.suptitle("Joint torques"); fig2.suptitle("Joint position"); fig3.suptitle("Joint velocity")
    # fig5.suptitle("End effector distance to the target position"); fig6.suptitle("MPC plans & real trajectory")

    # Set legends
    legend1 = [Line2D([0], [0], color='black', lw=1.5, label='Torque'),
            Line2D([0], [0], marker='o', color='b', label='Change appears', markerfacecolor='b', markersize=5)]
    fig1.legend(handles=legend1, ncol=2, loc="upper center")

    legend2 = [Line2D([0], [0],  lw=2, label='Joint position'),
            Line2D([0], [0], marker='o', color='b', label='Change appears', markerfacecolor='b', markersize=5),
            Line2D([0], [0], marker='o', color='r', label='DDP fail', markerfacecolor='r', markersize=5)]
    axs2[0,1].legend(handles=legend2)
    legend3 = [Line2D([0], [0],  lw=2, label='Joint velocity'),
            Line2D([0], [0], marker='o', color='b', label='Change appears', markerfacecolor='b', markersize=5),
            Line2D([0], [0], marker='o', color='r', label='DDP fail', markerfacecolor='r', markersize=5)]
    axs3[0,1].legend(handles=legend3)
    legend5 = [Line2D([0], [0],  lw=1.5, color='black', label='Distance to target'),
            Line2D([0], [0], marker='o', color='b', label='Change appears', markerfacecolor='b', markersize=5)]
    axs5.legend(handles=legend5)

    pa1 = Patch(facecolor='xkcd:barney purple'); pa2 = Patch(facecolor='xkcd:light eggplant'); pa3 = Patch(facecolor='xkcd:ugly purple')
    pb = Patch(facecolor='black'); dummy = Patch(facecolor='white')
    blue_circle = Line2D([0], [0], marker='o', color='b', label='Change appears', markerfacecolor='b', markersize=5)
    
    fig6.legend(handles=[pa1, pa2, pa3, pb, pb, pb, blue_circle],
            labels=['','', 'State plans','', '', 'State trajectory', 'Change appears'],
            ncol=7, handletextpad=0.5, handlelength=2, handleheight=0.3, columnspacing=0.0, loc = "upper center")
    
### Robust study plots ###
def OCP_study(t, joint_names, tau, tau_arm, tau_exo, joint_pos, joint_vel, error_q_arm, error_q_exo, ff_tau_arm, ff_tau_exo, fb_tau_arm, fb_tau_exo, error_eef):
    """Plot results of 1 OCP for a 4D robot.

    ----Parameters----
    joint_names : list of strings of joint names from URDF file
    tau, joint_pos, joint_vel, error_q, ff_tau, fb_tau, error_eef : 2D matrices of values for plotting for each of 4 joints"""
    joint_names=["Shoulder Yaw","Shoulder Pitch","Shoulder Roll","Elbow Pitch"]
    rad_to_deg = 180.0/3.141592653589793  

    fig1, axs1 = plt.subplots(2, 4, figsize=plt.figaspect(0.3)) # Joint torque
    fig2, axs2 = plt.subplots(1, 4, figsize=plt.figaspect(0.3)) # FF and FB Torque
    fig3, axs3 = plt.subplots(1, 4, figsize=plt.figaspect(0.3)) # Joint position error (x - x_ref)
    fig4, axs4 = plt.subplots(1, 4, figsize=plt.figaspect(0.3)) # Joint pos
    fig5, axs5 = plt.subplots(1, 4, figsize=plt.figaspect(0.3)) # Joint vel

    fig6, axs6 = plt.subplots() # EEF error

    aa = 1 # alpha
    cc_exo = 'xkcd:barney purple'
    cc_arm = 'xkcd:orange'
    cc_res = 'xkcd:midnight'

    for j in range(4):
        axs1[1,j].plot(t, tau[j,:], linewidth=1.5, color=cc_res, alpha = aa)
        axs1[0,j].plot(t, 0.5*tau_arm[j,:], linewidth=1.5, color=cc_arm, alpha = aa)
        axs1[0,j].plot(t, 0.5*tau_exo[j,:], linewidth=1.5, color=cc_exo, alpha = aa)
        axs1[1,j].set(xlabel=r"Time [s]")
        axs1[0,j].set(title=joint_names[j])

        axs2[j].plot(t, 0.5*fb_tau_arm[j,:], linewidth=1.5, color=cc_arm, alpha = aa, linestyle='dotted')
        axs2[j].plot(t, 0.5*ff_tau_arm[j,:], linewidth=1.5, color=cc_arm, linestyle='--', alpha = aa)
        axs2[j].plot(t, 0.5*fb_tau_exo[j,:], linewidth=1.5, color=cc_exo, alpha = aa, linestyle='dotted')
        axs2[j].plot(t, 0.5*ff_tau_exo[j,:], linewidth=1.5, color=cc_exo, linestyle='--', alpha = aa)
        axs2[j].set(xlabel=r"Time [s]", title=joint_names[j])

        axs3[j].plot(t, error_q_arm[j,:]*rad_to_deg, linewidth=1.5, color='r', alpha = aa)
        axs3[j].plot(t, error_q_exo[j,:]*rad_to_deg, linewidth=1.5, color='r', linestyle='--', alpha = aa)
        axs3[j].set(xlabel=r"Time [s]", title=joint_names[j])

        axs4[j].plot(t, joint_pos[j,:]*rad_to_deg, color=cc_res, linewidth=1.0)
        axs4[j].set(xlabel=r"Time [s]", title=joint_names[j])

        axs5[j].plot(t, joint_vel[j,:]*rad_to_deg, color=cc_res, linewidth=1.0)
        axs5[j].set(xlabel=r"Time [s]", title=joint_names[j])

    axs1[0,0].set(ylabel = r"$\tau$ [Nm]")
    axs1[1,0].set(ylabel = r"$\tau$ [Nm]")
    axs2[0].set(ylabel = r"$\tau$ [Nm]")

    axs3[0].set(ylabel =r"Distance [deg]")
    axs4[0].set(ylabel = r"Position [deg]")
    axs5[0].set(ylabel = r"Velocity [$\frac{{deg}}{{s}}$]")

    axs6.plot(t, error_eef*100, color=cc_res, linewidth=1.5)
    axs6.set(xlabel=r"Time [s]", ylabel=r"Distance to target [cm]")

    # Legends
    legend = [Line2D([0], [0],  lw=2, label='Exoskeleton', color='xkcd:barney purple'),
            Line2D([0], [0],  lw=2, label='Arm', color='xkcd:orange'),
            Line2D([0], [0],  lw=2, label='Result', color='xkcd:midnight')]
    fig1.legend(handles=legend, loc='upper center', ncol=3)

    legend = [Line2D([0], [0],  lw=2, label=r"FB $\tau$ - Exoskeleton", color='xkcd:barney purple', linestyle='dotted'),
            Line2D([0], [0],  lw=2, label=r"FF $\tau$ - Exoskeleton", color='xkcd:barney purple', linestyle='dashed'),
            Line2D([0], [0],  lw=2, label=r"FB $\tau$ - Arm", color='xkcd:orange', linestyle='dotted'),
            Line2D([0], [0],  lw=2, label=r"FF $\tau$ - Arm", color='xkcd:orange', linestyle='dashed')]

    fig2.legend(handles=legend, loc='upper center', ncol=4)

    # fig1.suptitle("Joint torque"); fig2.suptitle("FF & FB torque")
    # fig3.suptitle("Offset between Pinnochio and pyBullet positions in each step"); fig4.suptitle("Joint position (Result)"); fig5.suptitle("Joint velocity (Result)")
    # fig6.suptitle("Open-Loop")

    fig1.savefig("fig_study_target_OCP_tau.pdf")
    fig2.savefig("fig_study_target_OCP_tau_2.pdf")
    fig3.savefig("offset_simulation_pinocchio.pdf")
    fig4.savefig("joint_pos.pdf")
    fig5.savefig("joint_vel.pdf")
    fig6.savefig("eef_error.pdf")

def MPC_study(MPC, tau, tau_arm, tau_exo, joint_pos, joint_vel, error_eef, MPC_plans_arm, MPC_plans_exo):
    """Plot states, control, MPC plans

    ----Parameters----
    MPC : instance of mpc class that contains reaching_task , T_simulation, pinT, mpcDT, pinDT
    torques, joint_pos, joint_vel, error_eef : 2D matrices of values for plotting for each of 4 joints
    MPC_plans : 3D Matrix that contains all MPC planned trajectories for 4 joints (shoulder 3D and elbow)"""
    cc_exo=['xkcd:barney purple', 'xkcd:light eggplant', 'xkcd:ugly purple', 'xkcd:purple', 'xkcd:orchid']
    cc_arm=['xkcd:orange', 'xkcd:faded orange', 'xkcd:tangerine', 'xkcd:pastel orange', 'xkcd:pumpkin']
    cc_res = 'xkcd:midnight'

    rad_to_deg = 180.0/3.141592653589793 
    subplot_titles=["Shoulder Yaw","Shoulder Pitch","Shoulder Roll","Elbow Pitch"]

    t_arm = np.arange(0, MPC.arm.T_sim, MPC.pybDT)
    t_exo = np.arange(0, MPC.exo.T_sim, MPC.pybDT)
    t = np.arange(0, max([MPC.arm.T_sim, MPC.exo.T_sim]), MPC.pybDT)

    # Define figures
    fig1, axs1 = plt.subplots(2, 4, figsize=plt.figaspect(0.4)) # Joint torques
    fig2, axs2 = plt.subplots(2, 4, figsize=plt.figaspect(0.4)) # Joint positions and vel
    fig5, axs5 = plt.subplots()     # End effector error
    fig6, axs6 = plt.subplots(1, 4, figsize=plt.figaspect(0.3))  # MPC trajectories

    for j in range(4): # Go through 4 DoF
        #Plot torque
        axs1[0,j].plot(t_arm, 0.5*tau_arm[j,:], linewidth=1.5, color=cc_arm[0])
        axs1[0,j].plot(t_exo, 0.5*tau_exo[j,:], linewidth=1.5, color=cc_exo[0])
        axs1[1,j].plot(t, tau[j,:], linewidth=1.5, color=cc_res)
        axs1[0,j].set(title=subplot_titles[j])
        axs1[1,j].set(xlabel=r"Time [s]")

        # Plot joint position
        axs2[0,j].plot(t, joint_pos[j,:]*rad_to_deg, linewidth=1.5, color=cc_res)
        axs2[0,j].set(title=subplot_titles[j])

        # Plot joint velocity
        axs2[1,j].plot(t, joint_vel[j,:]*rad_to_deg, linewidth=1.5, color=cc_res)
        axs2[1,j].set(xlabel=r"Time [s]")

    axs1[0,0].set(ylabel = r"$\tau$ [Nm]")
    axs1[1,0].set(ylabel = r"$\tau$ [Nm]")

    axs2[0,0].set(ylabel = r"Position [deg]")
    axs2[1,0].set(ylabel = r"Velocity [$\frac{\mathrm{deg}}{\mathrm{s}}$]")

    # Legends
    legend = [Line2D([0], [0],  lw=2, label='Exoskeleton', color='xkcd:barney purple'),
            Line2D([0], [0],  lw=2, label='Arm', color='xkcd:orange'),
            Line2D([0], [0],  lw=2, label='Result', color='xkcd:midnight')]
    fig1.legend(handles=legend, loc='upper center', ncol=3)

    # Plot distance to target
    axs5.plot(t, error_eef*100, color=cc_res, linewidth=1.5)
    axs5.set(xlabel=r"Time [s]", ylabel=r"Distance to target [cm]")

    # Plot MPC trajecotries - elbow, shoulder x,y,z
    horizon_arm = MPC.arm.DT*(MPC.arm.T) # Moving horizon
    horizon_exo = MPC.exo.DT*(MPC.exo.T) 

    for i in range(MPC.number_replans_arm):
        t_mpc_arm = np.linspace(i*MPC.arm.mpcDT, round((i*MPC.arm.mpcDT + horizon_arm), 2) , MPC.arm.T)
        axs6[0].plot(t_mpc_arm, MPC_plans_arm[i,:,0]*rad_to_deg, linewidth=1.5, color=cc_arm[i%len(cc_arm)], alpha=0.3) # shoulder x
        axs6[1].plot(t_mpc_arm, MPC_plans_arm[i,:,1]*rad_to_deg, linewidth=1.5, color=cc_arm[i%len(cc_arm)], alpha=0.3) # shoulder y
        axs6[2].plot(t_mpc_arm, MPC_plans_arm[i,:,2]*rad_to_deg, linewidth=1.5, color=cc_arm[i%len(cc_arm)], alpha=0.3) # shoulder z
        axs6[3].plot(t_mpc_arm, MPC_plans_arm[i,:,3]*rad_to_deg, linewidth=1.5, color=cc_arm[i%len(cc_arm)], alpha=0.3) # elbow 
    for i in range(MPC.number_replans_exo):
        t_mpc_exo = np.linspace(i*MPC.exo.mpcDT, round((i*MPC.exo.mpcDT + horizon_exo), 2) , MPC.exo.T)
        axs6[0].plot(t_mpc_exo, MPC_plans_exo[i,:,0]*rad_to_deg, linewidth=1.0, color=cc_exo[i%len(cc_exo)], alpha=0.2) # shoulder x
        axs6[1].plot(t_mpc_exo, MPC_plans_exo[i,:,1]*rad_to_deg, linewidth=1.0, color=cc_exo[i%len(cc_exo)], alpha=0.2) # shoulder y
        axs6[2].plot(t_mpc_exo, MPC_plans_exo[i,:,2]*rad_to_deg, linewidth=1.0, color=cc_exo[i%len(cc_exo)], alpha=0.2) # shoulder z
        axs6[3].plot(t_mpc_exo, MPC_plans_exo[i,:,3]*rad_to_deg, linewidth=1.0, color=cc_exo[i%len(cc_exo)], alpha=0.2) # elbow
    
    axs6[0].set(xlabel=r"Time [s]", title = 'Shoulder Yaw', ylabel = r"Position [deg]",)
    axs6[1].set(xlabel=r"Time [s]", title = 'Shoulder Pitch')
    axs6[2].set(xlabel=r"Time [s]", title = 'Shoulder Roll')
    axs6[3].set(xlabel=r"Time [s]", title='Elbow Pitch')

    axs6[0].plot(t, joint_pos[0,:]*rad_to_deg, linewidth=1.5, color=cc_res) # Add real trajectory over the MPC plans
    axs6[1].plot(t, joint_pos[1,:]*rad_to_deg, linewidth=1.5, color=cc_res)
    axs6[2].plot(t, joint_pos[2,:]*rad_to_deg, linewidth=1.5, color=cc_res)
    axs6[3].plot(t, joint_pos[3,:]*rad_to_deg, linewidth=1.5, color=cc_res)   

    # Set legends
    pa1 = Patch(facecolor='xkcd:bright purple'); pa2 = Patch(facecolor='xkcd:light eggplant'); pa3 = Patch(facecolor='xkcd:ugly purple')
    pa11 = Patch(facecolor='xkcd:orange'); pa22 = Patch(facecolor='xkcd:tangerine'); pa33 = Patch(facecolor='xkcd:pale orange')
    pb = Patch(facecolor='xkcd:midnight')

    fig6.legend(handles=[pa1, pa2, pa3, pa11, pa22, pa33, pb, pb, pb],
            labels=['','', 'Exoskeleton plans','', '', 'Arm plans','', '', 'State trajectory'],
            ncol=9, handletextpad=0.5, handlelength=2, handleheight=0.3, columnspacing=0.0, loc = "upper center")

    # Set figure titles
    # fig1.suptitle("Joint torques"); fig2.suptitle("Joint position (Result)")
    # fig5.suptitle("MPC")
    # fig6.suptitle("MPC plans & real trajectory")

    fig1.savefig("joint_tor.pdf")
    fig2.savefig("joint_state.pdf")
    fig5.savefig("eef_err.pdf")
    fig6.savefig("mpc_traj.pdf")
    plt.show()