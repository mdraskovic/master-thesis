## Learning and Control for Exoskeletons

This project is a part of a bigger US National Science Foundation (NSF) project called Improving the Future of Retail and Warehouse Workers with Upper Limb Disabilities via Perceptive and Adaptive Soft Wearable Robots. The goal of this project is to design an upper limb exoskeleton which will assist people with physical disabilities in jobs involving picking, placing, and assembly tasks. Ultimately, this will enhance their productivity, inclusion and integration in work that is relevant to retail, warehouse and manufacturing.  


- Design and implement an inverse optimal control algorithm to learn objective functions from human demonstrations to capture reaching and manipulation movements. 
- The learned objective functions are subsequently used in a model predictive control (MPC) loop to control motions of an upper limb exoskeleton. If we consider dynamic and uncertain environment where the exoskeleton is performing, MPC is suitable as it frequently updates motion predictions based on the sensory feedback. The final result is the exoskeleton controller which is able to produce torque that comfortably assists the wearer in executing a reaching task towards a recorded target
